<?php
session_start();

// Composer -- Pakt voorbeeld Classes, Models, enz. -- Je classes & models universeel kan oproepen.
require 'vendor/autoload.php';

// Support -- Maakt View toegankelijk in alle Controllers/Models.
require 'core/support.php';


// Algemene Connnectie voor PDO & toegangsleutels.
App::bind('config', require 'config.php');
App::bind('query', Connection::make(App::get('config')['database']));

//
Router::load('routes.php')->direct(Request::uri(), Request::method()); //chain methods


