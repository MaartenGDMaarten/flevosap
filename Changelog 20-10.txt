(Ik moet dit bestand nog in Markdown maken)

***PrototypeV0.7*** - 20-10-2022

*Summary:*
Completely revamped our whole project to fit the Routing, Controller and CRUD systems taught to us by Stephan. 
The only feature which I didn't manage to migrate was the Shopping Cart.
Fixed about 20 huge bugs (which I was kidding).

I made substantial changes to the login system for improved performance.
Changing your password now keeps you logged in.
I have also introduced User Types (Admin = 1, Consumer = 2, Company = 3) which the system properly recognizes and grands/limits access accordingly.
For example, the NavBar won't show an Admin button if you are not an admin. Attempting to go to Localhost:1234/admin will result in a redirect back to the index page if you aren't an admin.
Introduced Hashed Passwords everywhere.

Created an Admin Dashboard which for now has two main pages. Product Page and User Page.
You can add/create, change, delete and get an overview of products or users on their respective pages.
And so so so much more I will add tomorrow and reupload the file.



To-do:
- Automatic login upon account creation.
- Part 2 of the Password Recovery feature.
- Create Specific Product Pages based on User Input
- Introduce email on account creation
- Introduce email customization to admin dashboard



Jeff - Details:

***Controllers***
Added:
- AccountController: New
- AdminController: New.
- PagesController: New.
- ProductController: New.
- UserController: New.

***Models***
Changed:
- ProductModel: Completely revamped in combination with ProductController
- UserModel: Completely revamped in combination with UserController


***Routes***
Added
- 28 extra Routes.

(Will add more later.. it's too late)





Friday (13): 


Fri – Jeff: Winkelwagen gefixed (colab Tobie) 

Fri – Jeff: Navbar Winkelwagen en winkelwagen in Adminbar koppelen 

Fri – Jeff: Product toevoegen aan winkelwagen andere pagina 

Fri – Jeff: Hamburger Trigger fix 
Fri – Jeff: Hamburger fix applied to all pages 

Fri – Jeff: 404 NOT FOUND system added 

Fri – Jeff: Login, Add Email to Create 

Fri – Jeff: Add Email column to AdminUser 

Fri – Jeff: Add Email to AdminUser Create 

Fri – Jeff: Add Email to AdminUser Update 

Fri – Jeff: Product, add Picture 

Fri – Jeff: Auto-Login upon Account Creation 

Fri – Jeff: Continue US Routing 

Fri – Jeff: Continue US Revamp Login System 

Fri – Jeff: Continue US Admin Dashboard (currently on impediment because I need Tobie & Faissals User Stories (filter&order list) 

Fri – Jeff: Continue US Password Recovery – It works, only must add an email server 
 

Saturday (12): 

Sat – Jeff: Productpagina inladen aan de hand van database query (denk aan Product Update mechanisme.) 

Sat – Jeff: Producten aan de winkelwagen toevoegen via Product Specific 

 

Sunday (11): 

Sun – Jeff: Winkelwagen revamp inclusief company excl btw 

Sun – Jeff: Productspecifiek pagina company prijs excl btw 

Sun – Jeff: ProductOverzicht andere data inladen aan de hand van User ID (company/consumer/guest) 

Sun – Jeff: Database Product Table verder aanvullen 

Sun – Jeff: Database Product Table verder aanvullen 

Sun – Jeff: Hoeveelheid van product in winkelwagen reflecteren op productpagina 

Sun – Jeff: Bestseller met shuffle (max 3 volgens consult) 

Sun – Jeff: Bestseller bij Product Admin toegevoegd 

Sun – Jeff: US Webshop interface voor consument + zakelijk 

Merged all SQL files into one and fixed duplicate keys..


 
