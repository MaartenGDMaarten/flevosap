<?php

class PagesController {

    public function __construct() {
        $this->product = new ProductModel();
    }


    public function home() {

        $products = $this->product->showAll();

        return view('index', compact('products'));
    }

    public function about() {
        return view('about');
    }

    public function contact() {
        return view('contact');
    }

    public function cart() {
        return view('cart');
    }

    public function notFound() {
        return view('not_found');
    }

    public function overview() {
        return view('overview');
    }

    public function payment() {
        return view('payment');
    }

    public function place_order() {
        return view('place_order');
    }

}