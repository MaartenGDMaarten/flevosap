<?php

class AccountController
{

    public function __construct()
    {
        $this->user = new UserModel();
    }

    public function home()
    {

        $users = $this->user->showAll();

        return view('user', compact('users'));
    }

    public function add()
    {

        $this->user->add();

        header("Location: gebruikers");   //REP
    }

    public function delete()
    {
        $this->user->delete();

        if (@$_SESSION["User_type_id"] <> 1) {
            header("Location: /");
        } else {
            header("Location: gebruikers");
        }
    }

    public function update()
    {
        $user = $this->user->update();

        return view('admin/user-upd', compact('user'));   //REP
    }

    public function store()
    {
        $this->user->store();

        if (@$_SESSION["User_type_id"] <> 1) {
            header("Location: /");
        } else {
            header("Location: gebruikers");
        }

    }

    public function login()
    {
        require 'views/account/login.php';
    }

    public function registreer()
    {
        require 'views/account/register.php';
    }

    public function forgotPass()
    {
        require 'views/account/forgot_password.php';
    }

    public function welcome()
    {

        $user = $this->user->updateProfile();

//        return view('user', compact('users'));
//        return view('account/welcome', compact('user'));
        require 'views/account/welcome.php';
    }

    public function logout()
    {
        require 'views/account/logout.php';
    }

    public function reset()
    {
        require 'views/account/passwordreset.php';
    }

    public function resetrequest()
    {
        require 'views/account/forgot_password_reset.php';
    }


}

?>