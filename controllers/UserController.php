<?php

class UserController
{

    public function __construct()
    {
        $this->user = new UserModel();
    }

    public function home()
    {

        $users = $this->user->showAll();

        return view('user', compact('users'));
    }

    public function add()
    {

        $this->user->add();

        header("Location: gebruikers");   //REP
    }

    public function delete()
    {
        $this->user->delete();

        if (@$_SESSION["User_type_id"] <> 1) {
            header("Location: /");
        } else {
            header("Location: gebruikers");
        }
    }

    public function update()
    {
        $user = $this->user->update();

        return view('admin/user-upd', compact('user'));   //REP
    }

    public function updatePass()
    {
        $user = $this->user->updatePass();

        return view('admin/user-updpass', compact('user'));   //REP
    }

    public function store()
    {
        $this->user->store();

        header("Location: welcome");
    }

    public function storeAdmin()
    {
        $this->user->storeAdmin();

        if (@$_SESSION["User_type_id"] <> 1) {
            header("Location: /");
        } else {
            header("Location: gebruikers");
        }

    }

    public function storePass()
    {
        $this->user->storePass();

        header("Location: gebruikers");
    }
}