<?php

class AdminController
{

    public function __construct() {

        $this->product = new ProductModel();
        $this->user = new UserModel();
    }

    public function home()
    {
        $products = $this->product->showAll();
        $users = $this->user->showAll();
        return view('admin/admin', compact('products', ('users')));
    }

    public function producten()
    {
        $products = $this->product->showAll();
        $users = $this->user->showAll();
        return view('admin/admin_product', compact('products', ('users')));
    }

    public function gebruikers()
    {
        $products = $this->product->showAll();
        $users = $this->user->showAll();
        return view('admin/admin_user', compact('products', ('users')));
    }

    public function bestellingen()
    {
        $products = $this->product->showAll();
        $users = $this->user->showAll();
        return view('admin/admin_order', compact('products', ('users')));
    }

    public function help()
    {
        $products = $this->product->showAll();
        $users = $this->user->showAll();
        return view('admin/admin_help', compact('products', ('users')));
    }

    public function addProduct()
    {
        return view('admin/product-add');
    }

    public function addUser()
    {
        return view('admin/user-add');
    }

//    public function analytics()
//    {
//        $products = $this->product->showAll();
//        $users = $this->user->showAll();
//        return view('admin/admin_analytic', compact('products', ('users')));
//    }

//    public function admin_admin()
//    {
//        $products = $this->product->showAll();
//        $users = $this->user->showAll();
//        return view('admin/admin_admin', compact('products', ('users')));
//    }

//    public function faq()
//    {
//        $products = $this->product->showAll();
//        $users = $this->user->showAll();
//        return view('admin/admin_faq', compact('products', ('users')));
//    }


}

?>