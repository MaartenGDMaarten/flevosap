<?php

class ProductController {


    public function __construct() {
        $this->product = new ProductModel();
    }

    public function home() {

        $products = $this->product->showAll();

        return view('product_overview', compact('products'));
    }

    public function geen_match() {

        return view('no_search_match');
    }

    public function redirect() {

        $products = $this->product->showAll();

        return view('product_overview', compact('products'));
    }

    public function add()
    {

        $this->product->add();

        header("Location: producten");
    }

    public function delete()
    {
        $this->product->delete();

        if (@$_SESSION["User_type_id"] <> 1) {
            header("Location: productoverzicht");
        }
        else{
            header("Location: producten");
        }
    }

    public function update()
    {
        $product = $this->product->update();

        return view('admin/product-upd', compact('product'));
    }

    public function store()
    {
        $this->product->store();

        if (@$_SESSION["User_type_id"] <> 1) {
            header("Location: productoverzicht");
        }
        else{
            header("Location: producten");
        }

    }

    public function details()
    {
        $product = $this->product->details();

        return view('product_specific', compact('product'));
    }

    public function filterAdmin()
    {
        $productsF = $this->product->filterAdmin();

        return view('admin/admin_product', compact('productsF'));
    }

    public function filterUser()
    {
        $productsF = $this->product->filterUser();

        return view('product_overview', compact('productsF'));
    }


//    ----------------------- //
    public function filterStock()
    {
        $productsF = $this->product->filterStock();

        return view('admin/admin_product', compact('productsF'));
    }
    //    ----------------------- //

    public function filterSearch()
    {
        $productsF = $this->product->filterSearch();

        return view('admin/admin_product', compact('productsF'));
    }

    public function filterSearchUser()
    {
        $productsF = $this->product->filterSearch();

        return view('product_overview', compact('productsF'));
    }


}

?>