<?php

class ContactformController {


    public function __construct() {
        $this->contact = new ContactformModel();
    }

    public function home() {

        $contacts = $this->contact->showAll();

        return view('contact_overview', compact('contacts'));
    }

//    public function add()
//    {
//
//        $this->contact->add();
//
//        header("Location: contacten");
//    }
//
//    public function delete()
//    {
//        $this->contact->delete();
//
//        if (@$_SESSION["User_type_id"] <> 1) {
//            header("Location: contactoverzicht");
//        }
//        else{
//            header("Location: contacten");
//        }
//    }
//
//    public function update()
//    {
//        $contact = $this->contact->update();
//
//        return view('admin/contact-upd', compact('contact'));
//    }
//
//    public function store()
//    {
//        $this->contact->store();
//
//        if (@$_SESSION["User_type_id"] <> 1) {
//            header("Location: contactoverzicht");
//        }
//        else{
//            header("Location: contacten");
//        }
//
//    }



}

?>