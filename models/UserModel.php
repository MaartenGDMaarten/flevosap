<?php

class UserModel
{

    protected $conn;

    public function __construct()
    {
        $this->conn = App::get('query');
    }

    public function showAll()
    {
        $sql = "SELECT * FROM users";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }


    public function add()
    {
        $hash = password_hash($_POST['password'], PASSWORD_BCRYPT);

        $sql = "INSERT INTO users (username, password, email, User_type_id)
                VALUES('{$_POST['userName']}', '$hash', '{$_POST['email']}', '{$_POST['userTypeId']}')";


        $stmt = $this->conn->prepare($sql);

        $stmt->execute();

    }

    public function delete()
    {
        // DIT IS NU HARD DELETE, UITZOEKEN HOE JE EEN SOFT DELETE KOMT?
        $sql = "DELETE FROM users WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    public function update()
    {
        $sql = "SELECT * FROM users WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function updatePass()
    {
        $sql = "SELECT * FROM users WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function store()
    {
        $sql = "UPDATE users SET 
                    username = '{$_POST['userName']}', 
                    email = '{$_POST['email']}',     
                    updated_at = current_timestamp WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    public function storeAdmin()
    {
        $sql = "UPDATE users SET 
                    username = '{$_POST['userName']}', 
                    User_type_id = '{$_POST['userTypeId']}', 
                    email = '{$_POST['email']}',     
                    updated_at = current_timestamp WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    public function updateProfile()
    {
        $sql = "SELECT * FROM users WHERE User_id = {$_SESSION['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function storePass()
    {

        $hash = password_hash($_POST['password'], PASSWORD_BCRYPT);

        $sql = "UPDATE users SET 
                    password = '$hash',  
                    updated_at = current_timestamp 
             WHERE User_id = {$_POST['userid']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }


}

?>
