<?php

class ProductModel
{
    protected $conn;

    public function __construct()
    {
        $this->conn = App::get('query');
    }

    public function showAll()
    {
        $sql = "SELECT * FROM products";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function add()
    {
        $sql = "INSERT INTO products (product, unit_price, stock, badge_bestseller, Category_id, unit, img, img_2, img_3, img_4, img_5)
                VALUES('{$_POST['productName']}', 
                       '{$_POST['productPrice']}', 
                       '{$_POST['productStock']}', 
                       '{$_POST['productBestseller']}', 
                       '{$_POST['productCategory']}', 
                       '{$_POST['productUnit']}',
                       '{$_POST['productImg']}',
                       '{$_POST['productImg2']}',
                       '{$_POST['productImg3']}',
                       '{$_POST['productImg4']}',
                       '{$_POST['productImg5']}'
                       
                       )
                       ";

        $stmt = $this->conn->prepare($sql);

        $stmt->execute();

    }

    public function delete()
    {
        // DIT IS NU HARD DELETE, UITZOEKEN HOE JE EEN SOFT DELETE KOMT?
        $sql = "DELETE FROM products WHERE Product_id = {$_POST['id']}";//REP

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    public function update()
    {
        $sql = "SELECT * FROM products WHERE Product_id = {$_POST['id']}";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function store()
    {
        $sql = "UPDATE products SET 
                    product = '{$_POST['productName']}', 
                    unit_price = '{$_POST['productPrice']}',
                    stock = '{$_POST['productStock']}',
                    badge_bestseller = '{$_POST['productBestseller']}',
                    Category_id = '{$_POST['productCategory']}',
                    unit = '{$_POST['productUnit']}',
                    
                    updated_at = current_timestamp WHERE Product_id = {$_POST['productsid']}";//REP


        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
    }

    public function details()
    {

        if (empty($_POST['id'])){
            $sql = "SELECT * FROM products WHERE Product_id = {$_SESSION['redirect_id']}";
        } else {
            $sql = "SELECT * FROM products WHERE Product_id = {$_POST['id']}";
        }

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function filterAdmin()
    {
        $sql = "SELECT * FROM products WHERE {$_POST['ColumnName']} = {$_POST['RowValue']}";


        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function filterUser()
    {
        $sql = "SELECT * FROM products WHERE {$_POST['ColumnName']} = {$_POST['RowValue']}";


        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function filterStock()
    {
        $sql = "SELECT * FROM products WHERE {$_POST['ColumnName']} BETWEEN 0 AND {$_POST['RowValue']} ORDER BY {$_POST['ColumnName']} ASC";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    public function filterSearch()
    {
        $searchconcat = "'%" . $_POST['RowValue'] . "%'";

        $sql = "SELECT * FROM products WHERE {$_POST['ColumnName']} LIKE $searchconcat";


        $stmt = $this->conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }



}

?>