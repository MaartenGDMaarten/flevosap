<?php


//             /* "text" */ = Either the location or the condition upon which a route is used.

/* ---------------------- */
/* ----- Get routes ----- */
/* ---------------------- */

    //PagesController
$router->get('', 'PagesController@home'); /*navigation*/ /*admin_navigation*/
$router->get('over-ons', 'PagesController@about'); /*navigation*/
$router->get('contact', 'PagesController@contact'); /*navigation*/
$router->get('cart', 'PagesController@cart'); /*navigation*/ /*admin_navigation*/
$router->get('not_found', 'PagesController@notFound'); /* Gets trigger upon no products found during a search */
$router->get('overzicht', 'PagesController@overview'); /*navigation*/

$router->get('verzenden', 'PagesController@place_order'); // UIT VANWEGE POST /*navigation*/


    //AccountController
$router->get('user', 'AccountController@index'); /* Where? */
$router->get('login', 'AccountController@login'); /* Where? */
$router->get('welcome', 'AccountController@welcome'); /* Where? */
$router->get('logout', 'AccountController@logout'); /* Where? */
$router->get('reset', 'AccountController@reset'); /* Where? */
$router->get('resetrequest', 'AccountController@resetrequest'); /* Where? */
$router->get('registreer', 'AccountController@registreer'); /* Where? */
$router->get('forgot-pass', 'AccountController@forgotPass'); /* Where? */


    //ProductController
$router->get('product', 'ProductController@index'); /* Where? */
$router->get('productoverzicht', 'ProductController@home'); /* Where? */
$router->get('productpagina', 'ProductController@details'); // Used -- /*product_overzicht.view*/
$router->get('geen_match', 'ProductController@geen_match'); // Used -- /*product_overzicht.view*/

    //AdminController
$router->get('admin', 'AdminController@home'); /* Where? */
$router->get('producten', 'AdminController@producten'); /* Where? */
$router->get('gebruikers', 'AdminController@gebruikers'); /* Where? */
$router->get('bestellingen', 'AdminController@bestellingen'); /* Where? */
$router->get('help', 'AdminController@help'); /* Where? */
$router->get('admin-add-product', 'AdminController@addProduct'); /* Where? */
$router->get('admin-add-user', 'AdminController@addUser');  /* Where? */


/* ----------------------- */
/* ----- Post routes ----- */
/* ----------------------- */

    //PagesController
$router->post('verzenden', 'PagesController@place_order');
$router->post('betalen', 'PagesController@payment'); /*navigation*/

    //AccountController
$router->post('login', 'AccountController@login'); // Test if used
$router->post('welcome', 'AccountController@welcome'); // Test if used
$router->post('logout', 'AccountController@logout'); // Test if used
$router->post('reset', 'AccountController@reset'); // Test if used
$router->post('registreer', 'AccountController@registreer'); // Test if used
$router->post('forgot-pass', 'AccountController@forgotPass'); // Used
$router->post('resetrequest', 'AccountController@resetrequest'); // Test if used

    //ProductController
$router->post('add-product', 'ProductController@add'); // // Test if used
$router->post('del-product', 'ProductController@delete'); // Test if used
$router->post('upd-product', 'ProductController@update'); // Test if used
$router->post('store-product', 'ProductController@store'); //  /* Where? */
$router->post('productoverzicht', 'ProductController@home'); // Used - /*product_overzicht.view*/ /*no_search_match.view*/
$router->post('redirect-cart', 'ProductController@redirect'); // ProductSpecific Cart redirect after posting product
$router->post('productpagina', 'ProductController@details'); // Used -- /*product_overzicht.view*/
$router->post('producten', 'ProductController@filterAdmin'); // Used -- /*admin_product.view*/
$router->post('productenfilter', 'ProductController@filterUser'); // Used -- /*product_overview.view*/
$router->post('productenstock', 'ProductController@filterStock'); // Used -- /*product_overzicht.view*/
$router->post('productensearch', 'ProductController@filterSearch'); // Used -- /*product_overzicht.view*/
$router->post('productoverzicht-search', 'ProductController@filterSearchUser'); // Used -- /*product_overzicht.view*/

    //UserController
$router->post('add-user', 'UserController@add'); // Test if used
$router->post('del-user', 'UserController@delete'); // Test if used
$router->post('upd-user', 'UserController@update'); // Test if used
$router->post('upd-userPass', 'UserController@updatePass'); // Test if used
$router->post('store-userAdmin', 'UserController@storeAdmin'); // Test if used
$router->post('store-user', 'UserController@store'); // Use this one for normal users
$router->post('store-userPass', 'UserController@storePass');

    //AdminController
$router->post('productenreset', 'AdminController@producten'); // Used -- /*admin_product.view*/

?>