<?php


// Include config file
require_once 'database/dbcontroller.php';
require_once 'config_mysqli.php';
require 'views/utils/navigation.php';


// Define variables and initialize with empty values
$username = $password = $confirm_password = $user_type_id = $email = "";
$username_err = $password_err = $confirm_password_err = $user_type_id_err = $email_err = "";

// Processing form data when form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    // Validate username
    if (empty(trim($_POST["username"]))) {
        $username_err = "Please enter a username.";
    } elseif (!preg_match('/^[a-zA-Z0-9_]+$/', trim($_POST["username"]))) {
        $username_err = "Username can only contain letters, numbers, and underscores.";
    } else {
        // Prepare a select statement
        $sql = "SELECT User_id FROM users WHERE username = ?";

        if ($stmt = mysqli_prepare($link, $sql)) {
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);

            // Set parameters
            $param_username = trim($_POST["username"]);

            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)) {
                /* store result */
                mysqli_stmt_store_result($stmt);

                if (mysqli_stmt_num_rows($stmt) == 1) {
                    $username_err = "This username is already taken.";
                } else {
                    $username = trim($_POST["username"]);
                }
            } else {
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }

    // Validate EMAIL
    if (empty(trim($_POST["email"]))) {
        $email_err = "Please enter an email.";
    } else {
        $email = trim($_POST["email"]);
    }

    // Validate password
    if (empty(trim($_POST["password"]))) {
        $password_err = "Please enter a password.";
    } elseif (strlen(trim($_POST["password"])) < 6) {
        $password_err = "Password must have atleast 6 characters.";
    } else {
        $password = trim($_POST["password"]);
    }

    // Validate confirm password
    if (empty(trim($_POST["confirm_password"]))) {
        $confirm_password_err = "Please confirm password.";
    } else {
        $confirm_password = trim($_POST["confirm_password"]);
        if (empty($password_err) && ($password != $confirm_password)) {
            $confirm_password_err = "Password did not match.";
        }
    }

    // Validate UserType
    if (@$_POST["company"]) {
        $user_type_id = ($_POST["company"]);
    } else {
        $user_type_id = 2;
    }


    // Check input errors before inserting in database
    if (empty($username_err) && empty($password_err) && empty($confirm_password_err) && empty($email_err)) {

        // Prepare an insert statement
        $sql = "INSERT INTO users (username, password, User_type_id, email) VALUES (?, ?, ?, ?)";

        if ($stmt = mysqli_prepare($link, $sql)) {
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "ssss", $param_username, $param_password, $param_User_type_id, $param_email);

            // Set parameters
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
            $param_User_type_id = $user_type_id;
            $param_email = $email;

            // Attempt to execute the prepared statement
            if (mysqli_stmt_execute($stmt)) {
                // Redirect to login page

                // Store data in session variables
                $id = mysqli_insert_id($link);

                $_SESSION["loggedin"] = true;
                $_SESSION["userid"] = $id;
                $_SESSION["username"] = $username;
                $_SESSION["User_type_id"] = $user_type_id;

                header("location: login");
            } else {
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }

    // Close connection
    mysqli_close($link);
}
?>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/main.css">

    <!-- JQuery for Checkbox Toggle -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo_appel.webp"> <!-- IMG in browser tab -->
    <title>Aanmelden</title>
</head>
<body>

<script>
    $(document).ready(function () {
        $('#company').change(function () {
            $('#mycheckboxdiv').toggle();
            $('#mycheckboxdiv2').toggle();
        });
    });
</script>


<form action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]); ?>" method="post">
    <div class="four_in_row">
        <div class="container py-3">


            <h2>Aanmelden</h2>
            <div class="row p-1">
                <div class="col-lg-6 col-md-6 col-sm-6 col-12 p-3">
                    <label>Gebruikers</label>
                    <input type="text" name="username"
                           class="form-control <?php echo (!empty($username_err)) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $username; ?>">
                    <span class="invalid-feedback"><?php echo $username_err; ?></span>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-12 p-3">
                    <label>Email</label>
                    <input type="text" name="email"
                           class="form-control <?php echo (!empty($email_err)) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $email; ?>">
                    <span class="invalid-feedback"><?php echo $email_err; ?></span>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-12 p-3">
                    <label>Wachtwoord</label>
                    <input type="password" name="password"
                           class="form-control <?php echo (!empty($password_err)) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $password; ?>">
                    <span class="invalid-feedback"><?php echo $password_err; ?></span>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-12 p-3">
                    <label>Wachtwoord bevestigen</label>
                    <input type="password" name="confirm_password"
                           class="form-control <?php echo (!empty($confirm_password_err)) ? 'is-invalid' : ''; ?>"
                           value="<?php echo $confirm_password; ?>">
                    <span class="invalid-feedback"><?php echo $confirm_password_err; ?></span>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-12 pb-5">
                <label><h3>Ben je een bedrijf?</h3></label><br>
                <input type="checkbox" id="company" name="company" value="3">
                <label for="company">Ik ben een bedrijf</label><br>
            </div>




            <h2>Optioneel</h2>


            <b>Naam</b>
            <div class="row p-1">

                <!-- First Name -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Voornaam</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>

                <!-- Last Name -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Achternaam</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>

                <!-- Initials -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Initialen</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>
            </div>


            <b>Contactgegevens</b>
            <div class="row p-1">

                <!-- Contact Email -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Contact Email</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>

                <!-- Phone Number -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Telefoonnummer</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>

            </div>

            <b>Adres</b>
            <div class="row p-1">
                <!-- Postal -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Postcode</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>

                <!-- City-->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Stad</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>

                <!-- Country -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Land</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>

                <!-- Province -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Provincie</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>

                <!-- Address -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Adres</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>

                <!-- Address number -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="" class="form-label">Huisnummer</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>

                <b>Tweede Adres</b>
                <div class="row p-1">
                    <!-- Postal Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Postcode</label>
                            <input type="text" class="form-control" name="" id=""
                                   value="">
                        </div>
                    </div>

                    <!-- City Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Stad</label>
                            <input type="text" class="form-control" name="" id=""
                                   value="">
                        </div>
                    </div>

                    <!-- Country Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Land</label>
                            <input type="text" class="form-control" name="" id=""
                                   value="">
                        </div>
                    </div>

                    <!-- Province Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Provincie</label>
                            <input type="text" class="form-control" name="" id=""
                                   value="">
                        </div>
                    </div>

                    <!-- Address Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Adres</label>
                            <input type="text" class="form-control" name="" id=""
                                   value="">
                        </div>
                    </div>

                    <!-- Address number Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Huisnummer</label>
                            <input type="text" class="form-control" name="" id=""
                                   value="">
                        </div>
                    </div>

                    <div id="mycheckboxdiv" style="display:none">

                        <b>Bedrijfsgegevens</b>
                        <div class="row p-1">
                            <!-- Company Name -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                                <div class="mb-3">
                                    <label for="" class="form-label">Bedrijfsnaam</label>
                                    <input type="text" class="form-control" name="" id=""
                                           value="">
                                </div>
                            </div>

                            <!-- Website -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                                <div class="mb-3">
                                    <label for="" class="form-label">Website</label>
                                    <input type="text" class="form-control" name="" id=""
                                           value="">
                                </div>
                            </div>


                            <!-- Job title (company only) -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                                <div class="mb-3">
                                    <label for="" class="form-label">Functietitel</label>
                                    <input type="text" class="form-control" name="" id=""
                                           value="">
                                </div>
                            </div>

                        </div>

                    </div>

                    <div id="mycheckboxdiv2" style="display:block">
                        <b>Geboortedatum</b>
                        <div class="row p-1">
                            <!-- Day of Birth -->
                            <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                                <div class="mb-3">
                                    <label for="" class="form-label">Geboortedatum</label>
                                    <input type="text" class="form-control" name="" id=""
                                           value="">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group"><br>
                        <input type="submit" class="btn btn-primary" value="Verzenden">
                        <input type="reset" class="btn btn-secondary ml-2" value="Reset">
                    </div>
                </div>
            </div>

        </div>
</form>


<p>Heb je een account? <a href="login">Inloggen</a>.</p>

</body>
</html>