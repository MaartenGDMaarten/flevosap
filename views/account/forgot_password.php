<?php
@session_start();
require 'views/utils/navigation.php';

if($_SERVER["REQUEST_METHOD"] == "POST") {
//Our connection details. // DIT VERWERKEN IN CONTROLLER
    $host = 'localhost';
    $user = 'root';
    $password = 'pigfrog';
    $database = 'flevosap';

//An options array.
//Set the error mode to "exceptions"
    $pdoOptions = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => false
    );

//Connect to MySQL database using PDO.
    $pdo = new PDO("mysql:host=$host;dbname=$database", $user, $password);
//Our connection details. // DIT VERWERKEN IN CONTROLLER


//Get the name that is being searched for.
    $email = isset($_POST['email']) ? trim($_POST['email']) : '';

//The simple SQL query that we will be running.
    $sql = "SELECT `User_id`, `email` FROM `users` WHERE `email` = :email";

//Prepare our SELECT statement.
    $statement = $pdo->prepare($sql);

//Bind the $name variable to our :name parameter.
    $statement->bindValue(':email', $email);

//Execute the SQL statement.
    $statement->execute();

//Fetch our result as an associative array.
    $userInfo = $statement->fetch(PDO::FETCH_ASSOC);

//If $userInfo is empty, it means that the submitted email
//address has not been found in our users table.
    if (empty($userInfo)) {
        echo 'That email address was not found in our system!';
        exit;
    }

//The user's email address and id.
    $userEmail = $userInfo['email'];
    $userId = $userInfo['User_id'];

//Create a secure token for this forgot password request.
    $token = openssl_random_pseudo_bytes(16);
    $token = bin2hex($token);

//Insert the request information
//into our password_reset_request table.

//The SQL statement.
$insertSql = "INSERT INTO password_reset_request
              (user_id, date_requested, token)
              VALUES
              (:user_id, :date_requested, :token)";

//Prepare our INSERT SQL statement.
$statement = $pdo->prepare($insertSql);

//Execute the statement and insert the data.
$statement->execute(array(
    "user_id" => $userId,
    "date_requested" => date("Y-m-d H:i:s"),
    "token" => $token
));

//Get the ID of the row we just inserted.
    $passwordRequestId = $pdo->lastInsertId();


//Create a link to the URL that will verify the
//forgot password request and allow the user to change their
//password.
    $verifyScript = 'localhost:8000/views/account/forgot_password_verify.php';

//The link that we will send the user via email.
    $linkToSend = $verifyScript . '?uid=' . $userId . '&id=' . $passwordRequestId . '&t=' . $token;

//    Email Test
//    mail("jf.tang@yahoo.com","Yup",$linkToSend);
//
}
?>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo_appel.webp"> <!-- IMG in browser tab -->
    <title>Login</title>

</head>
<body>

<form action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]); ?>" method="post">
    <div class="form-group">
        <label>Email</label><br>
        <input type="text" name="email" value="">
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Opvragen">
    </div>


    <?php if (! empty($linkToSend)){
        //Print out the email for the sake of this tutorial.
        echo "ALS TEST: De link hieronder wordt naar een email gestuurd, dit linked naar een pagina die de request verified." . "<br>" . "Deze code is altijd uniek:" . "<br>" . "$linkToSend";}
    ?>

</body>
</html>