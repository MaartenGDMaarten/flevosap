<?php

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login");
    exit;
}
require 'views/utils/navigation.php';

?>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/welcome.css">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo_appel.webp"> <!-- IMG in browser tab -->
    <title>Welkom</title>
</head>
<body style="background-color:seashell">
<p>

</p>

<?php
//die(var_dump());
?>

<div class="four_in_row">
    <div class="container py-3">
        <div class="row p-1">
            <h1 class="my-5">Hallo, <?= $user[0]->username; ?>. Welkom op je Profiel.</h1>
        </div>
    </div>
</div>

<form action="store-user" method="post">
    <div class="four_in_row">
        <div class="container py-2" id="con2">


            <h2>Account Algemeen</h2>
            <hr>
            <div class="row p-1" >

                <!-- Username -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="userName" class="form-label">Gebruikersnaam</label>
                        <input type="text" class="form-control" name="userName" id="userName"
                               value="<?= $user[0]->username; ?>" required>
                    </div>
                </div>
                <!-- Email -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                    <div class="mb-3">
                        <label for="email" class="form-label">Email</label>
                        <input type="text" class="form-control" name="email" id="email"
                               value="<?= $user[0]->email; ?>"
                               required>
                    </div>
                </div>

                <h2>Wachtwoord</h2>
            <div class="row p-1">
            <!-- Wachtwoord -->
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                <div class="mb-3">
                    <a href="reset" class="btn1">Verander je wachtwoord</a>
                </div>
            </div>
        </div>



            <?php if ($user[0]->User_type_id == 3) {?>

                <h2>Company</h2>
                <div class="row p-1">
                    <!-- Company Name -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Bedrijfsnaam</label>
                            <input type="text" class="form-control" name="" id=""
                                   value="">
                        </div>
                    </div>

                    <!-- Website -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Website</label>
                            <input type="text" class="form-control" name="" id=""
                                   value="">
                        </div>
                    </div>
                </div>

            <?php }?>

                <?php if ($user[0]->User_type_id == 2) {?>

                <h2>Geboortedag</h2>
                <div class="row p-1">


                    <!-- Day of Birth -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-3">
                        <div class="mb-3">
                            <label for="" class="form-label">Geboortedatum</label>
                            <input type="text" class="form-control" name="" id=""
                                   value="">
                        </div>
                    </div>
                </div>

            <?php } ?>

                <h2 class="h22">Names</h2>
            <div class="row p-1">

                <!-- First Name -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Voornaam</label>
                        <input type="text" class="form-control" name="" placeholder="Voornaam invoeren" id=""
                               value="">
                    </div>
                </div>

                <!-- Last Name -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Achternaam</label>
                        <input type="text" class="form-control" name="" placeholder="Achternaam invoeren"id=""
                               value="">
                    </div>
                </div>

                <!-- Initials -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Initialen</label>
                        <input type="text" class="form-control" name="" placeholder="Initialen invoeren"id=""
                               value="">
                    </div>
                </div>


                <?php if ($user[0]->User_type_id == 3) {?>
                <!-- Job title (company only) -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Functietitel</label>
                        <input type="text" class="form-control" name="" id=""
                               value="">
                    </div>
                </div>
            </div>
        <?php } ?>



                <h2>Contactgegevens</h2>
            <div class="row p-1">

                <!-- Contact Email -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Contact Email</label>
                        <input type="text" class="form-control" name="" placeholder="Naam@outlook.com"id=""
                               value="">
                    </div>
                </div>

                <!-- Phone Number -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Telefoonnummer</label>
                        <input type="text" class="form-control" name="" placeholder="+31"id=""
                               value="">
                    </div>
                </div>

            </div>

                <h2>Adres</h2>
            <div class="row p-1">
                <!-- Postal -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Postcode</label>
                        <input type="text" class="form-control" name="" placeholder="Postcode invoeren" id=""
                               value="">
                    </div>
                </div>

                <!-- City-->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Stad</label>
                        <input type="text" class="form-control" name="" placeholder="Stad invoeren" id=""
                               value="">
                    </div>
                </div>

                <!-- Country -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Land</label>
                        <input type="text" class="form-control" name="" placeholder="Land invoeren" id=""
                               value="">
                    </div>
                </div>

                <!-- Province -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Provincie</label>
                        <input type="text" class="form-control" name="" placeholder="Provincie invoeren" id=""
                               value="">
                    </div>
                </div>

                <!-- Address -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Adres</label>
                        <input type="text" class="form-control" name="" placeholder="Adres invoeren" id=""
                               value="">
                    </div>
                </div>

                <!-- Address number -->
                <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                    <div class="mb-3">
                        <label for="" class="form-label">Huisnummer</label>
                        <input type="text" class="form-control" name="" placeholder="Huisnummer invoeren"id=""
                               value="">
                    </div>
                </div>

                <h2>&nbsp;Tweede Adres</h2>
                <div class="row p-1">
                    <!-- Postal Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                        <div class="mb-3">
                            <label for="" class="form-label">Postcode</label>
                            <input type="text" class="form-control" name="" placeholder="Postcode ivoeren" id=""
                                   value="">
                        </div>
                    </div>

                    <!-- City Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                        <div class="mb-3">
                            <label for="" class="form-label">Stad</label>
                            <input type="text" class="form-control" name="" placeholder="Stad invoeren" id=""
                                   value="">
                        </div>
                    </div>

                    <!-- Country Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                        <div class="mb-3">
                            <label for="" class="form-label">Land</label>
                            <input type="text" class="form-control" name="" placeholder="Land invoeren" id=""
                                   value="">
                        </div>
                    </div>

                    <!-- Province Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                        <div class="mb-3">
                            <label for="" class="form-label">Provincie</label>
                            <input type="text" class="form-control" name="" placeholder="Provincie invoeren" id=""
                                   value="">
                        </div>
                    </div>

                    <!-- Address Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                        <div class="mb-3">
                            <label for="" class="form-label">Adres</label>
                            <input type="text" class="form-control" name="" placeholder="Adres invoeren" id=""
                                   value="">
                        </div>
                    </div>

                    <!-- Address number Secondary -->
                    <div class="col-lg-3 col-md-6 col-sm-6 col-12 p-5">
                        <div class="mb-3">
                            <label for="" class="form-label">Huisnummer</label>
                            <input type="text" class="form-control" name="" placeholder="Huisnummer invoeren" id=""
                                   value="">
                        </div>
                    </div>


                    <div class="mb-3">
                        <input type="hidden" class="form-control" name="userid" id="userid"
                               value="<?= $user[0]->User_id; ?>">
                    </div>
                    <button type="submit" class="btn2" >Submit</button>


                </div>
            </div>
        </div>
</form>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>

