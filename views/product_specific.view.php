<?php
@session_start();

@include_once("database/dbcontroller.php");
@include_once("../database/dbcontroller.php");

$db_handle = new DBController();

if (!empty($_GET["action"])) {
    switch ($_GET["action"]) {

        // Add item(s) to cart //
        case "add":
            if (!empty($_POST["quantity"])) {
                $productByCode = $db_handle->runQuery("SELECT * FROM products WHERE code='" . $_GET["code"] . "'");
                $itemArray = array($productByCode[0]["code"] => array('product' => $productByCode[0]["product"], 'code' => $productByCode[0]["code"], 'quantity' => $_POST["quantity"], 'unit_price' => $productByCode[0]["unit_price"], 'img' => $productByCode[0]["img"]));

                // If session is NOT empty //
                if (!empty($_SESSION["cart_item"])) {
                    if (in_array($productByCode[0]["code"], array_keys($_SESSION["cart_item"]))) {
                        foreach ($_SESSION["cart_item"] as $k => $v) {
                            if ($productByCode[0]["code"] == $k) {
                                if (empty($_SESSION["cart_item"][$k]["quantity"])) {
                                    $_SESSION["cart_item"][$k]["quantity"] = 0;
                                }
                                $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                            }
                        }
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"], $itemArray);
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                }
            }
            $product = $db_handle->runQuery("SELECT * FROM products WHERE code='" . $_GET["code"] . "'");
            $_SESSION['redirect_id'] = $product[0]["Product_id"];

            header("Location: /productpagina");
            break;

    }
}
?>

<html lang="en">
<head>


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/main.css">
    <link rel="stylesheet" href="/views/css/product_specific.css">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo_appel.webp"> <!-- IMG in browser tab -->
    <title>Product Specific</title> <!-- Text in browser tab -->

</head>


<body>
<?php require 'utils/navigation.php'; ?>

<?php //if(@$_SESSION["User_type_id"] == 3){
//    echo 'Hello Company, DIT WEGHALEN VOOR DE PRESENTATIE';
//}
//else {
//    echo 'Hello Consumer/Admin/Guest, DIT WEGHALEN VOOR DE PRESENTATIE';
//}
//?>

<!-- Breakcrumb -->
<div id="breadcrumb">
    <div class="container m-0">
        <div class="col-md-6 col-12">
            <div class="row">
                <a href="productoverzicht"><-- Terug naar Product Overzicht</a>
            </div> <!-- Row -->
        </div> <!-- Col -->
    </div> <!-- Con -->
</div> <!--ID -->

<!-- Product Image -->
<div id="product_main">
    <div class="container-fluid py-3">
        <div class="row justify-content-center g-0">
            <div class="col-md-3 col-9">
                <img src="<?php echo $product[0]->img; ?>" class="img-fluid" alt="...">


                <!-- Badges -->
                <?php
                // Bestseller
                if ($product[0]->badge_bestseller == 0)
                    echo '<span class="badge bg-success">Bestseller</span>';

                // New
                if ($product[0]->badge_new == 1)
                    echo '<span class="badge bg-success">New</span>';


                // Unavailable
                if ($product[0]->badge_available == 1)
                    echo '<span class="badge bg-success">Available</span>';

                // Natural
                if ($product[0]->badge_natural == 1)
                    echo '<span class="badge bg-success">Natural</span>';
                ?>
            </div> <!-- Col -->
        </div> <!-- Row -->
    </div> <!-- Con -->
</div> <!-- ID -->


<!-- /////////// ProductName /////////// CHANGE NAMMMEESSSSS-->
<div class="container pb-3">
    <div class="col-md-3 col-9">
        <?php echo $product[0]->product; ?>
    </div> <!-- Col -->
</div> <!-- Con -->


<!-- /////////// Product Price /////////// CHANGE NAMMMEESSSSS-->
<div class="container">
    <div class="col-md-3 col-9">
        Prijs: €<?php echo $product[0]->unit_price; ?>
    </div> <!-- Col -->
</div> <!-- Con -->


<?php if(@$_SESSION["User_type_id"] == 3){ ?>
    <!-- /////////// Product Price excl btw/////////// CHANGE NAMMMEESSSSS-->
<div class="container">
    <div class="col-md-3 col-9">
        Prijs (Excl. BTW): €<?php echo number_format(($product[0]->unit_price * (1 - 0.09)), 2); ?>
    </div> <!-- Col -->
</div> <!-- Con -->
<?php
} ?>

<!-- /////////// Volume /////////// CHANGE NAMMMEESSSSS-->
<div class="container">
    Inhoud: <?php echo $product[0]->unit_volume; ?><?php echo $product[0]->unit_volume_type; ?>
</div> <!-- Con -->

<?php if(@$_SESSION["User_type_id"] == 3){ ?>
    <!-- /////////// Eenheden per colli/////////// CHANGE NAMMMEESSSSS-->
    <div class="container">
        <div class="col-md-3 col-9 pb-3">
            Eenheden per collo: 6
        </div> <!-- Col -->
    </div> <!-- Con -->
    <?php
} ?>

<!-- /////////// ProductDescription /////////// CHANGE NAMMMEESSSSS-->
<div class="container pb-3">
    Product informatie: <br><?php echo $product[0]->product_description; ?>
</div> <!-- Con -->

<!-- /////////// Allergens /////////// CHANGE NAMMMEESSSSS-->
<div class="container">
    Allergenen: <?php echo $product[0]->allergen; ?>
</div> <!-- Con -->


<!-- /////////// Ingredients /////////// CHANGE NAMMMEESSSSS-->
<div class="container pb-3">
    Ingredienten: <?php echo $product[0]->ingredient; ?>
</div> <!-- Con -->


<!-- /////////// Usage /////////// CHANGE NAMMMEESSSSS-->
<div class="container">
    Gebruik: <?php echo $product[0]->instruction; ?>
</div> <!-- Con -->


<!-- /////////// Storage /////////// CHANGE NAMMMEESSSSS-->
<div class="container pb-3">
    Bewaren: <?php echo $product[0]->storage; ?>
</div> <!-- Con -->


<!-- /////////// NutritionLabel /////////// CHANGE NAMMMEESSSSS-->

<div id="nutrition_label">
    <div class="container pb-3">
        <table class="table table-sm">
            <thead>
            <tr>
                <th>Voedingstabel</th>
                <th>per 100<?php echo $product[0]->unit_volume_type; ?></th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>
                    Energie
                </td>
                <td>
                    <?php echo $product[0]->energy; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Totaal vet
                </td>
                <td>
                    <?php echo $product[0]->fat; ?>
                </td>
            </tr>
            <tr class="sub-item">
                <td>
                    waarvan Verzadigd vet
                </td>
                <td>
                    <?php echo $product[0]->s_fat; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Totaal koolhydraten
                </td>
                <td>
                    <?php echo $product[0]->carb; ?>
                </td>
            </tr>
            <tr class="sub-item">
                <td>
                    waarvan Suiker
                </td>
                <td>
                    <?php echo $product[0]->sugar; ?>
                </td>
            </tr>

            <tr>
                <td>
                    Eiwit
                </td>
                <td>
                    <?php echo $product[0]->protein; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Zout
                </td>
                <td>
                    <?php echo $product[0]->salt; ?>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<!-- /////////// Text 2 /////////// CHANGE NAMMMEESSSSS-->
<div class="container pb-3">
    Extra informatie: <br><?php echo $product[0]->extra_description; ?>
</div>





<?php require 'utils/bottom_bar.php'; ?>
<?php require 'utils/footer2.php'; ?>




</body>

</html>