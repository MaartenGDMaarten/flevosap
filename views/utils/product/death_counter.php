<?php
 echo '

<!-- Als we meer hebben om te promoten kunnen we dit als database inladen zoals de product cards, dus aan de hand van een database. -->
<div class="death_counter">
    <div class="container-fluid py-3">
        <div class="row justify-content-center g-0">
            <div class="col-lg-3 col-md-3 col-sm-5 col-5 p-3">
                <h1 class="card-title text-white">Het is hier best wel leeg..</h1>
                <br>
                <h8 class="card-text text-white">Het lijkt erop dat je iets zoekt waar we helaas niet zoveel van kunnen bieden.</h8>
                <br>
                <h8 class="card-text text-white">Wil je nog een keer zoeken, klik dan <a href="productoverzicht">hier</a>.</h8>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-5 col-7">
                <img src="/images/weinig_producten.jpg" class="img-fluid" alt="...">
            </div>
        </div>
    </div>
</div>
'

 ?>