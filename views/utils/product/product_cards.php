<?php

if (!empty($bestsellers)) {
    $i = 0;
    $title = 'Bestseller';
    $divclass = 'four_in_row';
    $head = 'normalhead';
} elseif ($category_check == 1) {
    $title = 'Fruitsappen';
    $divclass = 'four_in_row';
    $head = 'normalhead';
} elseif ($category_check == 2) {
    $title = 'Groentesappen';
    $divclass = 'red_in_row';
    $head = 'redhead';
} elseif ($category_check == 3) {
    $title = 'Ijsjes';
    $divclass = 'red_in_row';
    $head = 'redhead';
} elseif ($category_check == 4) {
    $title = 'Kleinverpakking';
    $divclass = 'red_in_row';
    $head = 'redhead';
} elseif ($category_check == 5) {
    $title = 'Voor de kids';
    $divclass = 'red_in_row';
    $head = 'redhead';
}

$counter = 0;


if (empty($bestsellers)) {
    foreach ($filter_switch as $product) :
        if ($product->unit == $product_type) {
            if ($product->Category_id == $category_check) {
                $counter++;
            }
        }
    endforeach;
} else {
    foreach ($bestsellers as $product) :
        if ($product->unit == $product_type) {
            if ($product->badge_bestseller == 1) {
                $counter++;
            }
        }
    endforeach;
}

?>

<div id="<?php echo "$head"; ?>">
    <?php if ($counter >= 1) { ?>
        <h2 class="text-center"><?php echo "$title"; ?></h2>
    <?php } else {
        $deathCounter++;
    } ?>
</div>

<div class="<?php echo "$divclass"; ?>">
    <div class="container">
        <div class="row justify-content-center p-1">


            <?php
            if (empty($bestsellers)) {
                //                Dit is voor de categorie rows
                foreach ($filter_switch as $product) :
                    if ($product->unit == $product_type) {
                        if ($product->Category_id == $category_check) { ?>

                            <div class="col-md-3 col-sm-5 col-6 p-2">
                                <div class="card h-100 text-center">

                                    <img src="<?php echo $product->img; ?>" alt="...">
                                    <div class="card-body">
                                        <h2 class="card-title"><?php echo $product->product; ?></h2>
                                        <p class="card-text">Per stuk: €<?php echo $product->unit_price; ?></p>
                                        <p class="card-text">Inhoud:
                                            <?php echo $product->unit_volume;
                                            echo $product->unit_volume_type; ?>
                                        </p>
                                        <form action="productpagina" method="post">
                                            <button type="submit" name="id" value="<?= $product->Product_id; ?>"
                                                    class="btn btn-primary">
                                                Shop
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>


                            <?php
                        }
                    }
                endforeach;
            } else {
//                Dit is voor de bestseller row
                shuffle($bestsellers);
                foreach ($bestsellers as $product) :
                    if ($product->unit == $product_type) {
                        if ($product->badge_bestseller == 1) {
                            if ($i++ > 2) break;
                            {

                                ?>


                                <div class="col-md-3 col-sm-5 col-6 p-2">
                                    <div class="card h-100 text-center">

                                        <img src="<?php echo $product->img; ?>" alt="...">
                                        <div class="card-body">
                                            <h2 class="card-title"><?php echo $product->product; ?></h2>
                                            <p class="card-text">Per stuk: €<?php echo $product->unit_price; ?></p>
                                            <p class="card-text">Inhoud:
                                                <?php echo $product->unit_volume;
                                                echo $product->unit_volume_type; ?>
                                            </p>
                                            <form action="productpagina" method="post">
                                                <button type="submit" name="id" value="<?= $product->Product_id; ?>"
                                                        class="btn btn-primary">
                                                    Shop
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                    }
                endforeach;
            }
            ?>


        </div>
    </div>
</div>