<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>test for,</title>
</head>
<body>
<section class="col-11 bg-secondary text-white p-3 m-5">
    <!-- contact txt -->
    <h1>Zoek contact</h1>
    <!-- name for info -->
    <div class="row mt-2">
        <div class="col">
          <input type="text" class="form-control" placeholder="Voornaam" aria-label="First name">
        </div>
        <div class="col">
          <input type="text" class="form-control" placeholder="Achternaam" aria-label="Last name">
        </div>
    </div>
    <!-- fill in email -->
    <div class="mb-3">
        <label for="exampleFormControlInput1" class="form-label">Email</label>
        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="flevosap@voorbeeld.nl">
      </div>
    <!-- add title/label -->
    <div class="mb-3">
        <label for="formGroupExampleInput" class="form-label">Onderwerp</label>
        <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Ik mis een product in de webshop.">
    </div>
    <!-- see faq to repport on -->
    <select class="form-select" aria-label="Default select example">
        <option selected>Categorie</option>
        <option value="FAQ 1 Probleem">FAQ 1 Probleem</option>
        <option value="FAQ 2 Probleem">FAQ 2 Probleem</option>
        <option value="FAQ 3 Probleem">FAQ 3 Probleem</option>
        <option value="FAQ 4 Probleem">FAQ 4 Probleem</option>
        <option value="Product">Product</option>
        <option value="Verzending">Verzending</option>
        <option value="Betaling">Betaling</option>
        <option value="Klacht">Klacht</option>
        <option value="Overig">Overig</option>
      </select>
    <!-- comment section -->
    <div class="form-floating">
        <textarea class="form-control mt-2" placeholder="Leave a comment here" id="floatingTextarea"></textarea>
        <label for="floatingTextarea">Comments</label>
    </div>
    <!-- submit buton -->
    <button type="button" class="btn btn-success mt-2 ">Success</button>
</section>

</body>
</html>