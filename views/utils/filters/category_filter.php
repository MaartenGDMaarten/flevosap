<div class="col-3 p-0">
<form action="<?php echo $form_action ?>" method="post">
    <button type="submit" class="form-control" name="RowValue" value="<?php echo $form_value ?>"
            class="btn btn-success">
        <?php echo $form_name ?>
    </button>
    <input type="hidden" name="ColumnName" value="<?php echo $form_column ?>">
</form>
</div>