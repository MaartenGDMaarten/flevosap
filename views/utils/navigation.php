<!-- Navigation -->
<style>
    @import url("https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css");

    a {
        color: darkred;
    }

    a:hover {
        color: indianred;

    nav {
        border-bottom-style: ridge;
        border-top-width: 1px;
        box-shadow: 2px 2px 5px rgba(80, 80, 80, 0.37);
    }
    .nav-item{
        display: flex;
        justify-content: flex-end ;
    }


</style>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
      integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

<nav class="navbar navbar-expand-md bg-light sticky-top">
    <div class="container-fluid">
        <a class="navbar-brand" href="/">

            <img src="/images/logo_appel.webp" alt="" width="32 " height="32" class="d-inline-block align-text-top">
            FlevoSap</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" href="productoverzicht">Winkel</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="over-ons">Over ons</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="contact">Contact</a>
                </li>

                <li class="nav-item" id="cart1">
                    <a href="cart"><i class="bi bi-basket fs-1 ms-2"></i></a>
                </li>
                <li class="nav-item" id="login1">
                    <a href="login"><i class="bi bi-person-circle fs-1 ms-3"></i></a>
                </li>



                <?php if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) { ?>

                    <li class="nav-item">
                        <a href="logout" class="btn btn-danger ml-3">Log uit</a>
                    </li>
                <?php } ?>


                <?php
                if (@$_SESSION["User_type_id"] == 1) { // Check User_type_id
                    echo '<li class="nav-item">
                    <a class="btn btn-info ml-3" href="producten">Admin</a>
                </li>';
                } ?>

            </ul>

        </div>
    </div>
</nav>

<!--Triggered Dropdown -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
        crossorigin="anonymous"></script>
<!-- End Navigation -->