<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo_appel.webp"> <!-- IMG in browser tab -->
    <title>Hello, Shipping!</title>
</head>
<body>

<!-- Navigation -->
<?php require 'views/utils/navigation.php'; ?>
<!-- End Navigation -->

<?php
//Classes
//include 'database/Connection.php';
//
////Our connection details. // DIT VERWERKEN IN CONTROLLER
//$host = 'localhost';
//$user = 'root';
//$password = 'pigfrog';
//$database = 'flevosap';
//
////Connect to database
//$conn = Connection::make();
//$conn = new PDO("mysql:host=$host;dbname=$database", $user, $password);

//Start session
@session_start();

// For loading Shippo
require_once('vendor/autoload.php');
Shippo::setApiKey("shippo_test_94be6883ca3aff64054ca0f35857e7d61a944bfe");

if (isset($_POST['name'])) {

    $name = $_POST['name'];
    $street = $_POST['street'];
    $city = $_POST['city'];
    $state = $_POST['state'];
    $zip = $_POST['zip'];
    $country = $_POST['country'];

    if (empty($name) || empty($street) || empty($city) || empty($state) || empty($zip) || empty($country)) {
        echo "All fields are required";
    }


}

//try {
//    $fromAddress = array(
//        'name' => 'John bibblediboop',
//        'street1' => '4388 Capitol Avenue',
//        'city' => 'Indianapolis',
//        'state' => 'Indiana',
//        'zip' => '46254',
//        'country' => 'United states'
//    );
//
//        $toAddress = array(
//            'name' => 'Shawn Ippotle',
//            'street1' => '2736 Joy Lane',
//            'city' => 'Burbank',
//            'state' => 'California',
//            'zip' => '91502',
//            'country' => 'United States',
//        );
//
//    $parcel = array(
//        'length'=> '5',
//        'width'=> '5',
//        'height'=> '5',
//        'distance_unit'=> 'in',
//        'weight'=> '2',
//        'mass_unit'=> 'lb',
//    );
//
//    $shipment = Shippo_Shipment::create( array(
//            'address_from'=> $fromAddress,
//            'address_to'=> $toAddress,
//            'parcels'=> array($parcel),
//            'async'=> false
//        )
//    );
//
//    $transaction = Shippo_Transaction::create( array(
//            'shipment' => $shipment,
//            'carrier_account' => '7fca0102bdeb45abbce89e9a76b5ab06',
//            'servicelevel_token' => 'ups_standard',
//        )
//    );
//
//    if ($transaction->status == 'SUCCESS') {
//        echo 'Track & Trace: ' . $transaction->tracking_url_provider;
//        echo 'Verzendlabel: ' . $transaction->label_url;
//    }
//} catch (Shippo_ApiError $e) {
//    echo '<br>' . $e->getMessage();
//}

//$accounts = Shippo_CarrierAccount::all();
//print_r($accounts);
//
//$accounts = Shippo_CarrierAccount::all(array('service_levels' => 1));
//print_r($accounts);
?>
<p>Click <a href="">here</a> if you want to use your saved shipping address.</p>

<script>
    document.querySelector('a').addEventListener('click', function(event) {
        event.preventDefault();

        document.querySelector('#name').value = 'Saved Name';
        document.querySelector('#street').value = 'Saved Street';
        document.querySelector('#city').value = 'Saved City';
        document.querySelector('#state').value = 'Saved State';
        document.querySelector('#zip').value = 'Saved Zip';
        document.querySelector('#country').value = 'Saved Country';
    });
</script>

<form action="betalen" method="post">

    <h3>Winkelwagen overzicht</h3>
    <div>
        <?php
        if (isset($_SESSION["cart_item"]))
        {

            foreach($_SESSION["cart_item"] as $row)
            {
                echo '<br>';
                echo "<img src='".$row['img']."' width='50' height='50'>" . '<br>';
                echo 'product: ' . $row['product'] . '<br>';
                echo 'code: ' . $row['code'] . '<br>';
                echo 'prijs: $' . $row['quantity']*$row['unit_price'] . '<br>';
                echo 'hoeveelheid: ' . $row['quantity'] . '<br>';
                echo '<br>';
            }
            echo 'totaal: ' . $_SESSION['total_quantity'] . '<br>';
            echo '$' . number_format($_SESSION['total_price'], 2);
        }
        else
        {
            echo 'Winkelwagen leeg';
        }
        ?>
    </div>

    <br><h3>Gebruiker info</h3>
    <label for="first_name">Voornaam:</label>
    <input type="text" name="first_name" id="first_name"><br>

    <label for="last_name">Achternaam:</label>
    <input type="text" name="last_name" id="last_name"><br><br>

    <input type="checkbox" id="company_checkbox" onclick="showDiv()" />
    <label for="company_checkbox">Ik heb een bedrijfsaccount</label><br><br>

    <div id="company_name_div">
        <label for="company_name">Bedrijfsnaam:</label><br>
        <input type="text" name="company_name" id="company_name"><br><br>
    </div>

    <script>
        document.getElementById('company_name_div').style.display = 'none';

        function showDiv() {
            if(document.getElementById('company_checkbox').checked) {
                document.getElementById('company_name_div').style.display = "block";
            }
            else {
                document.getElementById('company_name_div').style.display = "none";
            }
        }
    </script>


    <br><h3>Adres info</h3>
    <label for="postal">Postcode:</label>
    <input type="text" id="postal" name="postal"><br>

    <label for="address_number">Adresnummer:</label>
    <input type="text" id="address_number" name="address_number"><br><br>

    <label for="province">Province:</label><br>
    <select name="province" id="province">
        <option value="Drenthe">Drenthe</option>
        <option value="Flevoland">Flevoland</option>
        <option value="Friesland">Friesland</option>
        <option value="Gelderland">Gelderland</option>
        <option value="Groningen">Groningen</option>
        <option value="Limburg">Limburg</option>
        <option value="Noord-Brabant">Noord-Brabant</option>
        <option value="Noord-Holland">Noord-Holland</option>
        <option value="Overijssel">Overijssel</option>
        <option value="Utrecht">Utrecht</option>
        <option value="Zeeland">Zeeland</option>
        <option value="Zuid-Holland">Zuid-Holland</option>
    </select><br><br>

    <label for="city">Stad:</label><br>
    <input type="text" id="city" name="city"><br><br>

    <label for="address">Adres:</label>
    <input type="text" id="address" name="address"><br>

    <label for="address_secondary">Toevoeging:</label>
    <input type="text" id="address_secondary" name="address_secondary"><br><br>

    <div>
        <label for="instruction">Laat iets weten aan de bezorger:</label>
    </div>
    <div>
        <textarea name="instruction" id="instruction" cols="45" rows="2"></textarea>
    </div><br>

    <br><h3>Bezorg info</h3>

    <label for="date_preferred">Gewenste bezorgdatum:</label><br>
    <input type="date" id="date_preferred" name="date_preferred" /><br><br>

    <script>
        var today = new Date();
        var tomorrow = new Date();
        tomorrow.setDate(tomorrow.getDate() + 1);
        var tomorrowString = tomorrow.toISOString().split('T')[0];
        document.getElementById("date_preferred").setAttribute('min', tomorrowString);
    </script>

    <br><h3>Contact info</h3>

    <label for="contact_email">E-mailadres:</label><br>
    <input type="text" id="contact_email" name="contact_email"><br><br>

    <label for="contact_phone">Telefoonnummer:</label><br>
    <input type="tel" id="contact_phone" name="contact_phone"><br><br>

    <input type="submit" value="Bevestig">
</form>



<?php
if ($_POST['first_name'] != '') {


    //users
    if ($_POST['first_name'] == '') : $_POST['first_name'] = null; endif;
    if ($_POST['last_name'] == '') : $_POST['last_name'] = null; endif;

    //companies
    if ($_POST['company_name'] == '') : $_POST['company_name'] = null; endif;

    //addresses
    if ($_POST['postal'] == '') : $_POST['postal'] = null; endif;
    if ($_POST['address_number'] == '') : $_POST['address_number'] = null; endif;
    if ($_POST['province'] == '') : $_POST['province'] = null; endif;
    if ($_POST['city'] == '') : $_POST['city'] = null; endif;
    if ($_POST['address'] == '') : $_POST['address'] = null; endif;
    if ($_POST['address_secondary'] == '') : $_POST['address_secondary'] = null; endif;
    if ($_POST['instruction'] == '') : $_POST['instruction'] = null; endif;

    //shipments
    if ($_POST['date_preferred'] == '') : $_POST['date_preferred'] = null; endif;

    //contacts
    if ($_POST['contact_email'] == '') : $_POST['contact_email'] = null; endif;
    if ($_POST['contact_phone'] == '') : $_POST['contact_phone'] = null; endif;

//users
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];

    //companies
    $company_name = $_POST['company_name'];

    //addresses
    $postal = $_POST['postal'];
    $address_number = $_POST['address_number'];
    $province = $_POST['province'];
    $city = $_POST['city'];
    $address = $_POST['address'];
    $address_secondary = $_POST['address_secondary'];
    $instruction = $_POST['instruction'];

    //shipments
    $date_preferred = $_POST['date_preferred'];

    //contacts
    $contact_email = $_POST['contact_email'];
    $contact_phone = $_POST['contact_phone'];

//    if(empty($first_name) || empty($last_name)) {
//        header("Location: /place_order.view.php");
//        exit;
//    }

    try {
//  DATA TO CONNECT AND ADD: order_payments AND order_shipment AMONG OTHERS

    $conn->beginTransaction();

//  Generate order --- hardset User_id
    $conn->exec("INSERT INTO orders (User_id) VALUES (1)");
    $order_id = $conn->lastInsertId();

//  Add item
        foreach ($_SESSION["cart_item"] as $row) {
            $item_quantity = $row['quantity'];
            $item_product_id = $row['code'];
            $item_price = $row['unit_price'];
            $item_name = $row['product'];

            $conn->exec("INSERT INTO order_items (Order_id, code, quantity, unit_price, unit_tax, name) VALUES ('$order_id', '$item_product_id', '$item_quantity', '$item_price', 0.09, '$item_name')");

            $conn->exec("UPDATE products SET stock = stock - '$item_quantity' WHERE code = '$item_product_id'");
        }


//  Insert first_name and last_name into users --- hardset User_id and type
        $conn->exec("INSERT INTO names (User_id, Order_id, type, first_name, last_name) VALUES (1, '$order_id', 'user_consumer', '$first_name', '$last_name')");
////  Insert company_name into companies
        $conn->exec("INSERT INTO companies (name) VALUES ('$company_name')");
////  Insert postal, address_number, province, city, address, address_extra, instruction into addresses --- hardset User_id, country and type
        $conn->exec("INSERT INTO addresses (User_id, Order_id, country, type, postal, address_number, province, city, address, address_secondary, instruction) VALUES (1, '$order_id', 'Netherlands', 'shipping', '$postal', '$address_number', '$province', '$city', '$address', '$address_secondary', '$instruction')");
////  Insert contact_email and contact_phone into contacts --- hardset User_id, country and type
        $conn->exec("INSERT INTO contacts (User_id, Order_id, contact_email, contact_phone) VALUES (1, '$order_id', '$contact_email', '$contact_phone')");



        $conn->commit();
    } catch (PDOException $e) {


        $conn->rollBack();
        echo "Error: " . $e->getMessage();
    }
}
?>
<br><h3> Uw order ID </h3>
<?php


if (isset($order_id)) {
    unset($_SESSION["cart_item"]);
    echo 'Uw order ID is: ' . $order_id;
} ?>


<!-- Footer -->
<?php require 'views/utils/footer.php' ?>
<!-- End Footer -->

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
        crossorigin="anonymous"></script>
</body>