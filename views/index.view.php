<html lang="n1" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/home.css">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo_appel.webp"> <!-- IMG in browser tab -->
    <title>Flevosap!</title>
</head>
<body style="background-color:seashell">
<!-- Navigation -->

<?php require 'utils/navigation.php'; ?>
<!-- End Navigation -->
<!-- Intro met meest gewilde producten -->
<div>
    <!--    <div class="hero-image">-->
    <img src="/images/flevosap_banner.png" class="hero-image" width="100%" height="100%">
    <!--    </div>-->
</div>

<div class="content">
    <div class="container">
        <div class="title">
            <h3 class="h32">Liefhebbers van Flevosap: welkom!</h3>
            <p class="p1">De vele variaties zorgen voor de ene fruitige verrassing na de andere. De pure smaak van één
                soort, of een
                spannende combinatie van twee of meer. Altijd 100 procent natuurlijk,
                dus zonder smaak- en conserveringsmiddelen. Waar je ook bent, met Flevosap heb je altijd de smaak te
                pakken!</p>
        </div>
    </div>
    <br>
    <div class="container">
        <h3 class="h31">Meest Gewild!</h3>
        <div id="productsap">
            <div class="container mt-5">
                <h3 class="h36">Best Sellers</h3>
                <div class="row">
                    <div class="col-sm-3" id="test1">
                        <img src="images/huisstijl/Assortimentslider-peer-cranberry.png" width="80%">
                        <div class="card" style="width: 200px">
                            <div class="card-body">
                                <h5 class="card-title" id="tekst1">Appel aardbei</h5>
                                <p class="card-text">Voedingswaarde per 100 gram
                                    Energie: 194 kJ (46 kcal)
                                    Vetten: 0,0
                                <hr>
                                <div class="container">
                                    <form action="productpagina" method="post">
                                        <button class="button__background" type="submit" name="id"
                                                value="<?= $products[16]->Product_id; ?>">
                                            Koop nu!
                                        </button>
                                    </form>
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3" id="test3">
                        <img src="images/huisstijl/Assortimentslider-appel-aardbei.png" width="80%">
                        <div class="card" style="width: 200px">
                            <div class="card-body">
                                <h5 class="card-title" id="tekst1">Appel kers</h5>
                                <p class="card-text">Voedingswaarde per 100 gram
                                    Energie: 194 kJ (46 kcal)
                                    Vetten: 0,0
                                <hr>
                                <div class="container">
                                    <form action="productpagina" method="post">
                                        <button class="button__background" type="submit" name="id"
                                                value="<?= $products[18]->Product_id; ?>">
                                            Koop nu!
                                        </button>
                                    </form>
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3" id="test2">
                        <img src="images/huisstijl/Assortimentslider-appel-peer.png" width="80%">
                        <div class="card" style="width: 200px">
                            <div class="card-body">
                                <h5 class="card-title" id="tekst1">Appel peer</h5>
                                <p class="card-text">Voedingswaarde per 100 gram
                                    Energie: 194 kJ (46 kcal)
                                    Vetten: 0,0</p>
                                <hr>
                                <div class="container">
                                    <form action="productpagina" method="post">
                                        <button class="button__background" type="submit" name="id"
                                                value="<?= $products[4]->Product_id; ?>">
                                            Koop nu!
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-3" id="test4">
                        <img src="images/huisstijl/Assortimentslider-appel-sinaasappel.png" width="80%">
                        <div class="card" style="width: 200px">
                            <div class="card-body">
                                <h5 class="card-title" id="tekst1">Sinaasappel</h5>
                                <p class="card-text">Voedingswaarde per 100 gram
                                    Energie: 194 kJ (46 kcal)
                                    Vetten: 0,0
                                <hr>
                                <div class="container">
                                    <form action="productpagina" method="post">
                                        <button class="button__background" type="submit" name="id"
                                                value="<?= $products[22]->Product_id; ?>">
                                            Koop nu!
                                        </button>
                                    </form>
                                </div>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
    </div>
</div>
<!-- Einde intro met meest gewilde producten -->

<!-- Begin flevo nieuws -->

<br>
<br>
<section>
    <div class="container">
        <div class="title">
            <h3 class="h32">Nieuw bij Flevosap</h3>
            <p class="p1">Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout
                van een pagina,
                afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat het
                uit een min of meer normale verdeling
                van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer
                leesbaar nederlands maakt.</p>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-3" class="column">
                <img src="/images/verrassende.jpg" style="width:100%">
                <p>Verrassende smaken.</p>
            </div>
            <div class="col-sm-3" class="column">
                <img src="/images/flessen.jpg" style="width:100%">
                <p>Ook groot in te slaan!</p>
            </div>
            <div class="col-sm-3" class="column">
                <img src="/images/ontbijt.jpg" style="width:100%">
                <p>Sappen voor bij het ontbijt.</p>
            </div>
            <div class="col-sm-3" class="column">
                <img src="/images/flesjes.jpg" style="width:100%">
                <p>Elke slok een belevenis.</p>
            </div>
        </div>
    </div>

</section>

<!-- Einde flevo nieuws -->
<!-- Begin eind stuk -->
<section>
    <br>
    <br>

    <div class="container mt-5">
        <h3 class="h34">Altijd en overal genieten!</h3>

        <div class="row">
            <div class="col-sm-8">
                <p class="p2">
                    Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het bekijken van de layout
                    van een pagina,
                    uit een min of meer normale verdeling
                    van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer
                    leesbaar nederlands maakt.Het is al geruime tijd een bekend gegeven dat een lezer, tijdens het
                    bekijken van de layout
                    van een pagina,
                    afgeleid wordt door de tekstuele inhoud. Het belangrijke punt van het gebruik van Lorem Ipsum is dat
                    het
                    uit een min of meer normale verdeling
                    van letters bestaat, in tegenstelling tot "Hier uw tekst, hier uw tekst" wat het tot min of meer
                    leesbaar nederlands maakt.
                    De grote flessen kom je tegen in je supermarkt. Dat is makkelijk meenemen,
                    en kinderen zijn er dol op. Volwassenen trouwens ook. In de winkel vind je ook een handig
                    klein flesje dat graag mee onderweg gaat. Zoals naar (sport)school of werk, een dagje uit, of in je
                    rugzak op vakantie.

                    Gezellig samen wat eten en drinken in een café, lunchroom of restaurant? Ook daar kun je genieten
                    van
                    de smaak van vers fruit. Om zeker te weten dat je de echte krijgt, bestel je niet zo maar een sapje,
                    maar vraag je om Flevosap!


                </p>
            </div>
            <div class="col-sm-4">
                <img class="card-img-top" src="/images/huisstijl/lunchboxC.png" alt="Card image"
                     style="width: 100%">

            </div>
</section>
<!-- Eind stuk -->

<!--------------------------------------------------------------------------------------------->

<!--------------------------------------------------------------------------------------------->
</body>


</html>


<?php require 'utils/footer.php' ?>
<!-- End Footer -->

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
</body>
</html>