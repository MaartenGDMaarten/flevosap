<?php
@session_start();

/* Redirects to no_search_match.view if no hits are found upon filtering */
if (empty(@$products or $productsF)) {
    header('Location: geen_match');
}

/* Decides if Company or Consumer pages are loaded */
if (@$_SESSION["User_type_id"] == 3) {
    $product_type = 'tray';
} else {
    $product_type = 'ce';
}

/* Assists the search bar & filter */
if (empty($productsF)) {
    $filter_switch = $products;
} else {
    $filter_switch = $productsF;
}

/* We can use this to create a function @ the bottom of the page to give a message if NO/or a certain amount of products are found. */
/* To prevent a page that feels too empty */
$deathCounter = 0;
?>

<html lang="en">
<head>


    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/product_overview.css">

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo_appel.webp"> <!-- IMG in browser tab -->
    <title>Product Overzicht</title> <!-- Text in browser tab -->
</head>


<body>
<!-- /////////// Navbar ///////////-->
<?php require 'utils/navigation.php'; ?>

<!-- /////////// Homescreen ///////////-->
<div id="homescreen">
    <div class="container-fluid p-0">
        <picture>
            <source srcset="/images/huisstijl/twee_flesjes.jpg" media="(max-width: 767px)">
            <source srcset="/images/lekker_fruit.png">
            <img src="/images/lekker_fruit.png" alt="...">
        </picture>
    </div>
</div>


<!-- /////////// Filter & Search /////////// -->
<div id="searchbar">
    <div class="container py-3">
        <div class="row justify-content-center">
            <div class="col-md-6 col-12">

                <!-- Search Bar -->
                <?php
                $form_action = 'productoverzicht-search';
                require 'utils/filters/search.php';
                ?>

                <!-- Button Setup -->
                <?php
                $form_action = 'productenfilter';
                $form_column = 'Category_id';
                ?>

                <!--  Vanaf hier buttons positioning fixen              -->
                <div class="row">
                    <!-- Fruitsappen Filter Button -->
                    <?php
                    $form_value = 1;
                    $form_name = 'Fruitsappen';
                    require 'utils/filters/category_filter.php';
                    ?>

                    <!-- Show all Groentensappen -->
                    <?php
                    $form_value = 2;
                    $form_name = 'Groentensappen';
                    require 'utils/filters/category_filter.php';
                    ?>

                    <!-- Show all Icecreams-->
                    <?php
                    $form_value = 3;
                    $form_name = 'Isjes';
                    require 'utils/filters/category_filter.php';
                    ?>

                    <!-- Show all Bestsellers -->
                    <?php
                    $form_value = 1;
                    $form_name = 'Bestseller';
                    $form_column = 'badge_bestseller';
                    require 'utils/filters/category_filter.php';
                    ?>

                    <!-- Reset the product table -->
                    <?php
                    $form_action = 'productoverzicht';
                    $form_name = 'Reset';
                    require 'utils/filters/category_filter.php';
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>

<!-- /////////// Product Cards & Promo's /////////// -->
<!-- Bestseller Row -->
<?php
$bestsellers = $filter_switch;
require 'utils/product/product_cards.php';
$bestsellers = null;
?>

<!-- Small Promo -->
<?php require 'utils/product/small_promo.php' ?>

<!-- Fruit Row -->
<?php
$category_check = 1;
require 'utils/product/product_cards.php';
?>

<!-- Big Promo -->
<?php require 'utils/product/big_promo.php' ?>

<!-- Ijs Row -->
<?php
$category_check = 3;
require 'utils/product/product_cards.php';
?>

<!-- Groentensappen Row -->
<?php
$category_check = 2;
require 'utils/product/product_cards.php';
?>

<!-- Fruitsappen Row -->
<?php
$category_check = 5;
require 'utils/product/product_cards.php';
?>

<!-- Makkelijk mee te nemen Row -->
<?php
$category_check = 6;
require 'utils/product/product_cards.php';
?>

<!-- Death Counter-->
<?php
if ($deathCounter >= 4){
    require 'utils/product/death_counter.php';
}
?>


<!-- Twitter -->
<?php require 'utils/socialmedia/twitter.php'; ?>


<!-- Facebook -->
<?php require 'utils/socialmedia/facebook.php'; ?>


<!-- Footer -->
<?php require 'utils/footer.php'; ?>

</body>
</html>