<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo_appel.webp"> <!-- IMG in browser tab -->
    <title>Hello, Shipping!</title>
</head>
<body>

<!-- Navigation -->
<?php require 'views/utils/navigation.php'; ?>
<!-- End Navigation -->


<?php
$ID = 7;
if (isset($_POST['ID'])) {
    $ID = $_POST['ID'];
}
?>
<h1>Order <?php echo $ID;?></h1>
<?php
//order date
//(orders.Order_id)
//[(order.created_at)]

//BESTELDE ITEMS
//(order_items.code) (order_items.quantity) 'x' (order_items.name)          (order_items.price)

//BEZORGADRES
//<where type is shipping>
//(addresses.first_name) (addresses.last_name)
//(addresses.address) (addresses.address_number)
//(addresses.postal) ',' (addresses.city) (addresses.address)
//(addresses.country)

//[(order_shipment.carrier) (order_shipment.shipment_method)]

//FACTUURADRES
//[(order_payments.method)]

//<where type is billing>
//(addresses.first_name) (addresses.last_name)                          [(order_shipments.shipping_cost)]
//(addresses.address) (addresses.address_number)                        (order_items.unit_price)
//(addresses.postal) ',' (addresses.city) (addresses.address)           [(order_shipments.shipping_cost) + (order_items.unit_price)]
//(addresses.country)                                                   [(order_shipments.shipping_cost * order_shipments.tax_value)] + (order_items.unit_price * order_items.unit_tax)
//                                                                      []

//tables

?>
<div class="row">
	<div class="col-md-8">
		<h3>Bedankt!</h3>
		<hr>
		<p>Uw order is geplaatst!</p>
		<p>U ontvangt nog een mail met dit orderoverzicht.</p>
        <p>Selecteer een order:</p>

        <form action="" method="post">
        <select name="ID">
            <option value="" disabled selected>ID</option>
            <?php
            $query = "SELECT Order_id FROM flevosap.orders";

            $stmt = $conn->prepare($query);

            $stmt->execute();

            $orders = $stmt->fetchAll();

            foreach ($orders as $order) {
                echo '<option value="'.$order['Order_id'].'">'.$order['Order_id'].'</option>';
            }
            ?>
        </select>
        </form>
        <script>
                document.querySelector("select").addEventListener("change",function() {
                this.form.submit();
            });
        </script>

</body>
</html>
        </script>
        <?php



        $stmt = $conn->prepare(
            "SELECT
        orders.Order_id,
        order_items.code,
        order_items.quantity,
        order_items.name,
        order_items.unit_price,
        order_items.unit_tax

        FROM flevosap.orders
        INNER JOIN flevosap.order_items ON orders.Order_id = order_items.Order_id
        WHERE orders.Order_id = '$ID';"
        );

        $products = $stmt->execute();
        $products = $stmt->fetchAll();

        $stmt = $conn->prepare(
            "SELECT
        orders.Order_id,
        names.first_name,
        names.last_name,
        addresses.address,
        addresses.address_number,
        addresses.postal,
        addresses.city,
        addresses.address_secondary,
        addresses.country

        FROM flevosap.orders
        INNER JOIN flevosap.names ON orders.Order_id = names.Order_id
        INNER JOIN flevosap.addresses ON orders.Order_id = addresses.Order_id
        WHERE orders.Order_id = '$ID';"
        );

        $info = $stmt->execute();
        $info = $stmt->fetch();
        ?>


	</div>
</div>
<div class="row">
	<div class="col-md-4">
        <h3>Adressen</h3>
		<hr>
        <?php

//		$info = $_SESSION['info'];
//		unset($_SESSION['info']);


        echo '<h5>' . 'Factuuradres' .  '</h5>';
		echo '<p>' . $info['first_name'] . ' ' . $info['last_name'] . '</p>';
		echo '<p>' . $info['address'] . ' ' . $info['address_number'] . ' ' . $info['address_secondary'] . '</p>';
		echo '<p>' . $info['postal'] . ' ' . $info['city'] . '</p><br>';

        echo '<h5>' . 'Bezorgadres' .  '</h5>';
        echo '<p>' . $info['first_name'] . ' ' . $info['last_name'] . '</p>';
        echo '<p>' . $info['address'] . ' ' . $info['address_number'] . ' ' . $info['address_secondary'] . '</p>';
        echo '<p>' . $info['postal'] . ' ' . $info['city'] . '</p>';

		?>
    </div>
    <div class="col-md-4">
        <h3>Bestelling</h3>
        <hr>
        <?php
        echo '<table>';
        echo '<tr>';
        echo '<th>Product</th>';
        echo '<th></th>';
        echo '<th></th>';
        echo '<th>Prijs</th>';
        echo '<th>Btw</th>';
        echo '</tr>';

        foreach ($products as $key => $value) {
        echo '<tr>';
        echo '<td>' . $value["code"] . '</td>';
        echo '<td>' . $value["quantity"] . ' x' . '</td>';
        echo '<td>' . $value["name"] . '</td>';
        echo '<td>' . '€' . $value["unit_price"] . '</td>';
        echo '<td>' . $value["unit_tax"] . '%' . '</td>';
        echo '</tr>';
        }

        echo '</table><br>';



        ?>
        <h5>Prijs</h5>
        <?php
        $subtotal = 0;
        $subtotalWithTax = 0;

        foreach ($products as $product) {
            $subtotal += $product['quantity'] * $product['unit_price'];
            $subtotalWithTax += $product['quantity'] * ($product['unit_price'] * $product['unit_tax']);
        }

        echo '<p>' . 'Subtotaal: €'.round($subtotal, 2).'<br>' . '</p>';
        echo '<p>' . 'Btw: €'.round($subtotalWithTax, 2) . '</p>';
        echo '<p>' . 'Totaal: €'.round($subtotal + $subtotalWithTax, 2) . '</p>';
        ?>
    </div>
</div>


<!-- Footer -->
<?php require 'views/utils/footer.php' ?>
<!-- End Footer -->

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
        crossorigin="anonymous"></script>
</body>