<?php

//Check if the user is an admin || If admin = give access || If anything else -> redirect to Root)
if (@$_SESSION["User_type_id"] <> 1) { // Check User_type_id
header("Location: /"); // Redirects to root.
exit(); // Kill script
} ?>

<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


   <title>Product Updaten</title>

</head>
<body>
<!-- Navigation -->
<?php require 'views/utils/admin_navigation.php'; ?>
<!-- End navigation -->


<h2>---------------------------------------- <?= $product[0]->product; ?> -----------------------------------------</h2>
<form action="store-product" method="post">
    <div class="mb-3">
        <label for="productName" class="form-label">Product Naam</label>
        <input type="text" class="form-control w-25" name="productName" id="productName" value="<?= $product[0]->product; ?>">
    </div>
    <div class="mb-3">
        <label for="productPrice" class="form-label">Prijs</label>
        <input type="text" class="form-control w-25" name="productPrice" id="productPrice" value="<?= $product[0]->unit_price; ?>">
    </div>
    <div class="mb-3">
        <label for="productStock" class="form-label">Voorraad</label>
        <input type="text" class="form-control w-25" name="productStock" id="productStock" value="<?= $product[0]->stock; ?>">
    </div>
    <div class="mb-3">
        <label for="productBestseller" class="form-label">Bestseller (0 = Nee / 1 = Ja) </label>
        <input type="text" class="form-control w-25" name="productBestseller" id="productBestseller" value="<?= $product[0]->badge_bestseller; ?>">
    </div>
    <div class="mb-3">
        <label for="productCategory" class="form-label">Category (PLACEHOLDER)</label>
        <input type="text" class="form-control w-25" name="productCategory" id="productCategory" value="<?= $product[0]->Category_id; ?>">
    </div>
    <div class="mb-3">
        <label for="productUnit" class="form-label">Verpakkingstype</label>
        <input type="text" class="form-control w-25" name="productUnit" id="productUnit" value="<?= $product[0]->unit; ?>">
    </div>
    <div class="mb-3">
        <input type="hidden" class="form-control w-25" name="productsid" id="productsid" value="<?= $product[0]->Product_id; ?>">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>


</form>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>