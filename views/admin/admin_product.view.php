<?php

//Check if the user is an admin || If admin = give access || If anything else -> redirect to Root)
if (@$_SESSION["User_type_id"] <> 1) { // Check User_type_id
    header("Location: /"); // Redirects to root.
    exit(); // Kill script
}

?>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/admin_productview.css">

    <!-- BOOTSTRAP WORDT AL INGELADEN VIA NAVBAR -->
    <!-- BOOTSTRAP WORDT AL INGELADEN VIA NAVBAR -->
    <!-- BOOTSTRAP WORDT AL INGELADEN VIA NAVBAR -->


    <title>Producten</title>
</head>
<body style="background-color:seashell">
<?php require 'views/utils/admin_navigation.php'; ?>
<h1>Producten</h1>




<?php


// Base state
if (empty($productsF)) {
    $filter_switch = @$products;
} else {
    $filter_switch = $productsF;
}
?>



<a href="admin-add-product" class="btn3">Voeg product toe</a>
<div class="container">
    <div class="row">
        <div class="col-sm-3">
    <!-- Sort by stock amount -->
    <form action="productenstock" method="post" class="fl1">
        <input type="number" name="RowValue" placeholder="Laat minimum voorraad zien" value="">
        <input type="hidden" name="ColumnName" value="stock">
        <input type="submit" value="Filter">
    </form>
    <!-- Search Bar -->
    <div class="p-1">
        <form action="productensearch" method="post" class="fl1">
            <input type="text" name="RowValue" placeholder="Keyword" value="">
            <input type="hidden" name="ColumnName" value="keyword">
            <input type="submit" value="Filter"><br>
        </form>
    </div>
        </div>
        <div class="col-sm-12"
        <!-- Show all Fruitsappen -->
        <div class="">
            <form action="producten" method="post">
                <button type="submit" name="RowValue" value="1"
                        class="btn1">
                    Fruitsappen
                </button>
                <input type="hidden" name="ColumnName" value="Category_ID">
            </form>
        </div>

        <!-- Show all Groentensappen -->
        <div class="">
            <form action="producten" method="post">
                <button type="submit" name="RowValue" value="2"
                        class="btn1">
                    Groentensappen
                </button>
                <input type="hidden" name="ColumnName" value="Category_ID">
            </form>
        </div>

        <!-- Show all Icecreams-->
        <div class="">
            <form action="producten" method="post">
                <button type="submit" name="RowValue" value="3"
                        class="btn1">
                    Ijs
                </button>
                <input type="hidden" name="ColumnName" value="Category_ID">
            </form>
        </div>

        <!-- Show all Fruitsappen Klein products-->
        <div class="">
            <form action="producten" method="post">
                <button type="submit" name="RowValue" value="4"
                        class="btn1">
                    Fruitsappen Klein
                </button>
                <input type="hidden" name="ColumnName" value="Category_ID">
            </form>
        </div>

        <!-- Show all Children products-->
        <div class="">
            <form action="producten" method="post">
                <button type="submit" name="RowValue" value="6"
                        class="btn1">
                    Kinder
                </button>
                <input type="hidden" name="ColumnName" value="Category_ID">
            </form>
        </div>



        <!-- Show all Smoothie products-->
        <div class="">
            <form action="producten" method="post">
                <button type="submit" name="RowValue" value="7"
                        class="btn1">
                    Smoothie
                </button>
                <input type="hidden" name="ColumnName" value="Category_ID">
            </form>
        </div>

        <!-- Show all Consumer products-->
        <div class="">
            <form action="producten" method="post">
                <button type="submit" name="RowValue" value="'ce'"
                        class="btn1">
                    Consumer
                </button>
                <input type="hidden" name="ColumnName" value="unit">
            </form>
        </div>

        <!-- Show all Company products-->
        <div class="">
            <form action="producten" method="post">
                <button type="submit" name="RowValue" value="'tray'"
                        class="btn1">
                    Company
                </button>
                <input type="hidden" name="ColumnName" value="unit">
            </form>
        </div>

        <!-- Show all Bestsellers -->
        <div class="">
            <form action="producten" method="post">
                <button type="submit" name="RowValue" value="1"
                        class="btn1">
                    Bestseller
                </button>
                <input type="hidden" name="ColumnName" value="badge_bestseller">
            </form>
        </div>


        <!-- Reset the product table -->
        <div class="">
            <form action="productenreset" method="post">
                <button type="submit" name="RowValue"
                        class="btn2">
                    Reset
                </button>
                <input type="hidden" name="ColumnName">
            </form>
        </div>
    </div>
</div>






        <!-- Reset the product table -->

<?php if (empty($products)) { ?>
    <?php if (empty($productsF)) { ?>
        <div class="form-inline justify-content-center">
            <div class="p-1">
                <form action="productenreset" method="post">
                    <button type="submit" name="RowValue"
                            class="btn btn-danger">
                        <?php echo 'Geen Match, graag resetten en opnieuw proberen'; ?>
                    </button>
                    <input type="hidden" name="ColumnName">
                </form>
            </div>
        </div>
    <?php } ?>
<?php } ?>
<br>
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-11">
            <h2>ProductOverzicht</h2>

            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Product</th>
                        <th scope="col">Img</th>
                        <th scope="col">Prijs</th>
                        <th scope="col">Voorraad</th>
                        <th scope="col">Bestseller</th>
                        <th scope="col">Category</th>
                        <th scope="col">Verpakking</th>
                        <th scope="col">Update</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php
                    if (!empty($filter_switch)){
                    foreach ($filter_switch

                    as $product) : ?>


                    <tr>
                        <td><?= $product->Product_id; ?></td>
                        <td><?= $product->product; ?></td>
                        <td>
                            <?php if (!empty ($product->img)) {
                                echo '| <a target="_target" href=' . $product->img . '>1</a> |';
                            } ?>
                            <?php if (!empty ($product->img_2)) {
                                echo ' <a target="_target" href=' . $product->img_2 . '>2</a> |';
                            } ?>
                            <?php if (!empty ($product->img_3)) {
                                echo ' <a target="_target" href=' . $product->img_3 . '>3</a> |';
                            } ?>
                            <?php if (!empty ($product->img_4)) {
                                echo ' <a target="_target" href=' . $product->img_4 . '>4</a> |';
                            } ?>
                            <?php if (!empty ($product->img_5)) {
                                echo ' <a target="_target" href=' . $product->img_5 . '>5</a> |';
                            } ?>

                        </td>


                        <td><?= $product->unit_price; ?></td>
                        <td><?= $product->stock; ?></td>
                        <td><?= $product->badge_bestseller; ?></td>
                        <td><?= $product->Category_id; ?></td>
                        <td><?= $product->unit; ?></td>


                        <td>
                            <form action="upd-product" method="post">
                                <button type="submit" name="id" value="<?= $product->Product_id; ?>"
                                        class="btn btn-primary">
                                    UPDATE
                                </button>
                            </form>
                        </td>
                        <td>
                            <form action="del-product" method="post">
                                <button type="submit" name="id" value="<?= $product->Product_id; ?>"
                                        class="btn btn-danger">
                                    DELETE
                                </button>
                            </form>
                        </td>
                    </tr>
                    </tbody>
                    <?php

                    endforeach;
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>

</body>
</html>
