<?php

//Check if the user is an admin || If admin = give access || If anything else -> redirect to Root)
if (@$_SESSION["User_type_id"] <> 1) { // Check User_type_id
header("Location: /"); // Redirects to root.
exit(); // Kill script
}

?>

<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



    <title>Gebruiker toevoegen</title>

</head>
<body>
<!-- Navigation -->
<?php require 'views/utils/admin_navigation.php'; ?>
<!-- End navigation -->


<h2>---------------------------------------- Create User -----------------------------------------</h2>
<!--<h3>Alle velden correct invullen anders crash, moet het nog beveiligen (werkt wel bij STephan)</h3>-->

<form action="add-user" method="post">
    <div class="mb-3">
        <label for="userName" class="form-label">Username</label>
        <input type="text" class="form-control w-25" name="userName" id="userName" required>
    </div>
    <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control w-25" name="email" id="email" required>
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Wachtwoord Hashed</label>
        <input type="text" class="form-control w-25" name="password" id="password" required>
    </div>
    <div class="mb-3">
        <label for="userTypeId" class="form-label">User Type (1 = admin | 2 = Consumer | 3 = Company)</label>
        <input type="number" class="form-control w-25" name="userTypeId" id="userTypeId" min="1" max="3" required>
    </div>
    <div class="mb-3">
        <input type="hidden" class="form-control w-25" name="userid" id="userid">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>

</form>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>