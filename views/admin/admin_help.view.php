<?php


//Check if the user is an admin || If admin = give access || If anything else -> redirect to Root)
if (@$_SESSION["User_type_id"] <> 1) { // Check User_type_id
    header("Location: /"); // Redirects to root.
    exit(); // Kill script
}

?>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">
    <title>Help</title>
    <!-- CSS -->

    <link rel="stylesheet" href="/views/css/contact.css">
</head>
<body style="background-color:seashell">
<?php require 'views/utils/admin_navigation.php'; ?>

<!-- Frequently Asked Questions accordion -->
<section id="questions" class="p-5">
    <div class="container" id="con1">
        <h3 class="h31">Ondersteuning Admin</h3>
        <div class="accordion" id="accordion">

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading1">
                    <button class="p2 accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse1" aria-expanded="false" aria-controls="collapse1">

                       Admin hulptext
                    </button>
                </h2>
                <div id="collapse1" class="accordion-collapse collapse" aria-labelledby="heading1"
                     data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Flevosap sappen worden uitsluitend gemaakt van het beste fruit. Waar dit fruit vandaan komt, is seizoensgebonden,
                            en daarom komt ons fruit niet het hele jaar door van dezelfde fruitteler. Zo komen onze sinaasappels uit Brazilië, onze
                            mango’s uit India en Peru, onze appels uit onder andere Polen en Duitsland, en onze
                            tomaten uit Spanje en Frankrijk. Na de oogst worden de beste partijen door onze smaakexperts geselecteerd.</strong>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading2">
                    <button class="p2 accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse2"
                            aria-expanded="true" aria-controls="collapse2">
                        Admin hulptext
                    </button>
                </h2>
                <div id="collapse2" class="accordion-collapse collapse show" aria-labelledby="heading2"
                     data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Bij Flevosap bezoeken we op regelmatige basis de telers van onze vele fruitsoorten. We stellen hoge eisen aan de
                            kwaliteit van het fruit, maar daar staat tegenover dat duurzaamheid voor ons belangrijk is. Dit houdt in dat het fruit
                            geteeld moet worden met aandacht voor het milieu en dat de grond niet uitgeput wordt. Maar ook dat boeren een eerlijke
                            prijs voor het fruit krijgen en dat het personeel onder goede arbeidsvoorwaarden werkt. We werken er hard aan om al ons fruit
                            100% duurzaam te telen.
                            We werken met de andere fruitsoorten ook naar 100% toe en onze ambitie is om dit in 2030 bereikt te hebben.</strong>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading3">
                    <button class="p2 accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
                        Admin hulptext
                    </button>
                </h2>
                <div id="collapse3" class="accordion-collapse collapse" aria-labelledby="heading3"
                     data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>De sinaasappels worden bij de boomgaard verzameld en gaan vervolgens in vrachtwagens naar de fabriek. Het fruit wordt in het land van herkomst geperst.
                            Daarna wordt een deel van het water uit het sap gehaald door het sap korte tijd onder vacuüm te verwarmen en blijft er een ingedikt vloeibaar sap (concentraat) over.
                            Door het vacuüm gebeurt dit bij lagere temperatuur waardoor voedingsstoffen als vitaminen en de smaak goed bewaard blijven.
                            Concentreren zorgt er dus voor dat we de vruchten binnen 24 uur na de pluk kunnen persen, zodat smaak, vitaminen en mineralen zo optimaal mogelijk behouden blijven.
                            Het water wordt vervolgens van de pulp gescheiden, zodat we het water niet onnodig van A naar B hoeven te vervoeren, hierbij verminderen we het sap volume met 83%.
                            Hierdoor blijft van één liter sap ruim 800 ml water in het land van herkomst en wordt daar hergebruikt.
                            Het fruit kan zo dus op de plek groeien waar het van nature het best van smaak is en exact dezelfde hoeveelheid water als er in het land van herkomst wordt onttrokken,
                            wordt weer toegevoegd in Nederland. In totaal vermindert het gebruik van concentraat
                            de CO2-uitstoot met 39,5% vergeleken met sap dat geen concentraat gebruikt! Concentreren is dus ook veel minder belastend voor het milieu!</strong>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="heading4">
                    <button class="p2 accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
                        Admin hulptext
                    </button>
                </h2>
                <div id="collapse4" class="accordion-collapse collapse" aria-labelledby="heading4"
                     data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Vers sap, geperst sap en sap uit pak zijn allen 100% geperst fruit en bevatten dus dezelfde voedingsstoffen als fruit.
                            De hoeveelheden en verhoudingen verschillen echter wel. Voor een glas (200ml) Appelsientje worden gemiddeld drie sinaasappels geperst.
                            Hierdoor krijg je meer Vitamine C, kalium, foliumzuur en fruitsuikers binnen dan bij het eten van één sinaasappel.</strong>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="heading5">
                    <button class="p2 accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
                        Admin hulptext
                    </button>
                </h2>
                <div id="collapse5" class="accordion-collapse collapse" aria-labelledby="heading5"
                     data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Nee. Al onze producten zijn vrij van glutenbevattende granen, ei, pinda, noten, soja en melk (inclusief lactose).</strong>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="heading6">
                    <button class="p2 accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
                        Admin hulptext
                    </button>
                </h2>
                <div id="collapse6" class="accordion-collapse collapse" aria-labelledby="heading6"
                     data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong>Er worden geen dierlijke producten verwerkt in Flevosap en dus zijn de producten geschikt voor veganisten en vegetariërs. Ook bevat Flevosap
                            geen dierlijke gelatine. Bij het helder maken van Flevosap wordt er gebruik gemaakt van ultrafiltratie.
                            Ultrafiltreren is het filteren (zeven) van het sap door een heel fijn filter/zeef, waardoor de deeltjes achterblijven en er een helder sap ontstaat.</strong>
                    </div>
                </div>
            </div>
            <div class="accordion-item">
                <h2 class="accordion-header" id="heading7">
                    <button class="p2 accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
                        Huh?
                    </button>
                </h2>
                <div id="collapse7" class="accordion-collapse collapse" aria-labelledby="heading7"
                     data-bs-parent="#accordion">
                    <div class="accordion-body">
                        <strong><a href="/docs/Einddocumentatie.docx" download>Einddocumentatie 1</a></strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






</body>
</html>
