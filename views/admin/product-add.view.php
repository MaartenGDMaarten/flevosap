<?php

//Check if the user is an admin || If admin = give access || If anything else -> redirect to Root)
if (@$_SESSION["User_type_id"] <> 1) { // Check User_type_id
header("Location: /"); // Redirects to root.
exit(); // Kill script
}

?>

<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <title>Product Toevoegen</title>
</head>
<body>
<!-- Navigation -->
<?php require 'views/utils/admin_navigation.php'; ?>
<!-- End navigation -->


<h2>---------------------------------------- Voeg een image toe aan je folder -----------------------------------------</h2>
<form action="images/dbimg/uploadProduct.php" method="post" enctype="multipart/form-data">
    Select image to upload:
    <input type="file" name="fileToUpload" id="fileToUpload">
    <input type="submit" value="Upload Image" name="submit">
</form>

<?php
if (!empty(@$_SESSION['UploadMessage'])){
    echo 'De afbeelding is correct geupload.' . '<br>' . 'Gebruik <b> /images/dbimg/' . @$_SESSION['UploadMessage'] . ' </b> om het te gebruiken bij een product.' ;
    $autofillimg = '/images/dbimg/' . @$_SESSION['UploadMessage'];
    @$_SESSION['UploadMessage'] = null;
} ?>

<h2>---------------------------------------- Voeg Product Toe -----------------------------------------</h2>
<!--<h3>Alle velden correct invullen anders crash, moet het nog beveiligen (werkt wel bij STephan)</h3>-->
<form action="add-product" method="post">
    <div class="mb-3">
        <label for="productName" class="form-label">Product Naam</label>
        <input type="text" class="form-control w-25" name="productName" id="productName">
    </div>
    <div class="mb-3">
        <label for="productPrice" class="form-label">Prijs</label>
        <input type="text" class="form-control w-25" name="productPrice" id="productPrice" value="">
    </div>
    <div class="mb-3">
        <label for="productStock" class="form-label">Voorraad</label>
        <input type="text" class="form-control w-25" name="productStock" id="productStock" value="">
    </div>
    <div class="mb-3">
        <label for="productBestseller" class="form-label">Bestseller (0 = Nee / 1 = Ja) </label>
        <input type="text" class="form-control w-25" name="productBestseller" id="productBestseller" value="">
    </div>
    <div class="mb-3">
        <label for="productCategory" class="form-label">Category (PLACEHOLDER)</label>
        <input type="text" class="form-control w-25" name="productCategory" id="productCategory" value="">
    </div>
    <div class="mb-3">
        <label for="productUnit" class="form-label">Verpakkingstype</label>
        <input type="text" class="form-control w-25" name="productUnit" id="productUnit" value="">
    </div>
    <div class="mb-3">
        <label for="productImg" class="form-label">Afbeelding</label>
        <input type="text" class="form-control w-25" name="productImg" id="productImg" value="<?= @$autofillimg ?>">
    </div>
    <div class="mb-3">
        <label for="productImg2" class="form-label">Afbeelding 2</label>
        <input type="text" class="form-control w-25" name="productImg2" id="productImg2" value="">
    </div>
    <div class="mb-3">
        <label for="productImg3" class="form-label">Afbeelding 3</label>
        <input type="text" class="form-control w-25" name="productImg3" id="productImg3" value="">
    </div>
    <div class="mb-3">
        <label for="productImg4" class="form-label">Afbeelding 4</label>
        <input type="text" class="form-control w-25" name="productImg4" id="productImg4" value="">
    </div>
    <div class="mb-3">
        <label for="productImg5" class="form-label">Afbeelding 5</label>
        <input type="text" class="form-control w-25" name="productImg5" id="productImg5" value="">
    </div>
    <div class="mb-3">
        <input type="hidden" class="form-control w-25" name="productsid" id="productsid" value="">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>


</form>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>