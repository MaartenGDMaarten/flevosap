<?php

//Check if the user is an admin || If admin = give access || If anything else -> redirect to Root)
if (@$_SESSION["User_type_id"] <> 1) { // Check User_type_id
    header("Location: /"); // Redirects to root.
    exit(); // Kill script
}
?>



<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSS -->
    <link rel="stylesheet" href="/views/css/admin_user.css">

   <title>Gebruikers</title>
</head>
<body style="background-color:seashell">
<?php require 'views/utils/admin_navigation.php'; ?>

<h1>Gebruikers</h1>

<div class="container-fluid pb-5">
    <a href="admin-add-user" class="btn3">Create User</a>
</div>


<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h2>GebruikersOverzicht</h2>

            <div class="table-responsive">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">User</th>
                        <th scope="col">Email</th>
                        <th scope="col">User Type ID</th>

                        <th scope="col">Update</th>
                        <th scope="col">Update Password</th>
                        <th scope="col">Delete</th>
                    </tr>
                    </thead>

                    <tbody>
                    <?php foreach ($users as $user) : ?>
                    <tr>
                        <td><?= $user->User_id; ?></td>
                        <td><?= $user->username; ?></td>
                        <td><?= $user->email; ?></td>
                        <td><?= $user->User_type_id; ?></td>


                        <td>
                            <form action="upd-user" method="post">
                                <button type="submit" name="userid" value="<?= $user->User_id; ?>"
                                        class="btn btn-primary">
                                    UPDATE
                                </button>
                            </form>
                        </td>
                        <td>
                            <form action="upd-userPass" method="post">
                                <button type="submit" name="userid" value="<?= $user->User_id; ?>"
                                        class="btn btn-primary">
                                    Update Password
                                </button>
                            </form>
                        </td>
                        <td>
                            <form action="del-user" method="post">
                                <button type="submit" name="userid" value="<?= $user->User_id; ?>"
                                        class="btn btn-danger">
                                    DELETE
                                </button>
                            </form>
                        </td>

                    </tr>
                    </tbody>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</body>
</html>