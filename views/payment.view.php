<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo_appel.webp"> <!-- IMG in browser tab -->
    <title>Hello, Payment!</title>
</head>
<body>
<!-- Navigation -->
<?php require 'views/utils/navigation.php'; ?>
<!-- End Navigation -->

<?php
// autoload vendor for composer
require_once('vendor/autoload.php');

//Classes
include 'database/Connection.php';

//Start session
session_start();

\Stripe\Stripe::setApiKey('sk_test_51LryEyBISdLfEKClppLeMggNCSRecLG5tQKgsWIPQA4O8fCKf43thMHpGgktqsiAGtCr9ruyunu7HBhxIGH51cuV00qHZcNRYm');

// Possible better failure / success solution if website is publically available
//$endpoint = \Stripe\WebhookEndpoint::create([
//    'url' => 'http://localhost:8000/views/cart.php',
//    'enabled_events' => [
//        'charge.failed',
//        'charge.succeeded',
//    ],
//]);

// Create checkout session with API call to stripe, returned to $session variable
if (isset($_SESSION['total_price'])) {
    $session = \Stripe\Checkout\Session::create([
        'payment_method_types' => ['card', 'ideal'],
        'line_items' => [[
            'price_data' => [
                'currency' => 'eur',
                'product_data' => [
                    'name' => 'Uw bestelling',
                ],
                'unit_amount' => $_SESSION['total_price']*100,
            ],
            'quantity' => 1,
        ]],
        'mode' => 'payment',
        'success_url' => 'http://localhost:8000/overzicht',
        'cancel_url' => 'http://localhost:8000/overzicht',
    ]);
}

// Possible better failure / success output if website is publically available
//if ($endpoint->enabled_events == 'charge.succeeded') {
//    echo '<script>alert("The charge succeeded!");</script>';
//}
//elseif ($endpoint->enabled_events == 'charge.failed') {
//    echo '<script>alert("The charge failed!");</script>';
//}
?>




<html>
    <head>
        <title>Betaalpagina</title>
        <script src="https://js.stripe.com/v3/"></script>
    </head>
    <body>
        <button id="checkout-button">Afrekenen</button>
        <script>
            var stripe = Stripe('pk_test_51LryEyBISdLfEKCliSI5WmiO4Ri2qQv4o31tleu5rGstNrem5MzTCs41jV6QAkSPTLXV8i5A1Ey2Mg298bAysfnX00konkiUdt');
            const btn = document.getElementById("checkout-button")
            btn.addEventListener('click', function(e) {
                e.preventDefault();
                stripe.redirectToCheckout({
                    sessionId: "<?php echo $session->id; ?>"
                });
            });
        </script>

        <br><br>

    </body>
</html>
<!-- Footer -->
<?php require 'views/utils/footer.php' ?>
<!-- End Footer -->

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-u1OknCvxWvY5kfmNBILK2hRnQC3Pr17a+RTT6rIHI7NnikvbZlHgTPOOmMi466C8"
        crossorigin="anonymous"></script>
</body>