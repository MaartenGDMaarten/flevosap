<?php
@session_start();

@include_once("database/dbcontroller.php"); // Both are required due to cart not yet being routes friendly.
@include_once("../database/dbcontroller.php"); // Both are required due to cart not yet being routes friendly.

$db_handle = new DBController();
if(!empty($_GET["action"])) {
    switch($_GET["action"]) {

        // Add item(s) to cart //
        case "add":
            if(!empty($_POST["quantity"])) {
                $productByCode = $db_handle->runQuery("SELECT * FROM products WHERE code='" . $_GET["code"] . "'");
                $itemArray = array($productByCode[0]["code"]=>array('product'=>$productByCode[0]["product"], 'code'=>$productByCode[0]["code"], 'quantity'=>$_POST["quantity"], 'unit_price'=>$productByCode[0]["unit_price"], 'img'=>$productByCode[0]["img"]));

                // If session is NOT empty //
                if(!empty($_SESSION["cart_item"])) {
                    if(in_array($productByCode[0]["code"],array_keys($_SESSION["cart_item"]))) {
                        foreach($_SESSION["cart_item"] as $k => $v) {
                            if($productByCode[0]["code"] == $k) {
                                if(empty($_SESSION["cart_item"][$k]["quantity"])) {
                                    $_SESSION["cart_item"][$k]["quantity"] = 0;
                                }
                                $_SESSION["cart_item"][$k]["quantity"] += $_POST["quantity"];
                            }
                        }
                    } else {
                        $_SESSION["cart_item"] = array_merge($_SESSION["cart_item"],$itemArray);
                    }
                } else {
                    $_SESSION["cart_item"] = $itemArray;
                }
            }
            header("Location: /cart");
            break;



            // Remove one item //
        case "remove":
            if(!empty($_SESSION["cart_item"])) {
                foreach($_SESSION["cart_item"] as $k => $v) {
                    if($_GET["code"] == $k)
                        unset($_SESSION["cart_item"][$k]);
                    if(empty($_SESSION["cart_item"]))
                        unset($_SESSION["cart_item"]);
                }
            }
            header("Location: /cart");
            break;

            // Clear Cart//
        case "empty":
            unset($_SESSION["cart_item"]);
            header("Location: /cart");
            break;
    }
}
?>
<HTML>
<HEAD>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-iYQeCzEYFbKjA/T2uDLTpkwGzCiq6soy8tYaI1GyVh/UjpbCx/TYkiZhlZB6+fzT" crossorigin="anonymous">

    <!-- CSS -->
    <link href="views/css/cart_style.css" type="text/css" rel="stylesheet" />
    <link href="../views/css/cart_style.css" type="text/css" rel="stylesheet" />

    <!-- Other Head Items -->
    <link rel="icon" type="image/x-icon" href="/images/logo_appel.webp"> <!-- IMG in browser tab -->


    <TITLE>Winkelwagen</TITLE>

</HEAD>
<BODY>

<?php require 'utils/navigation.php'; ?>



<div id="shopping-cart">
    <div class="txt-heading">Winkelwagen</div>

    <a id="btnEmpty" href="/views/cart.view.php?action=empty">Winkelwagen leegmaken</a>
    <?php
    if(isset($_SESSION["cart_item"])){
        $total_quantity = 0;
        $total_price = 0;
        $_SESSION['total_quantity'] = 0;
        $_SESSION['total_price'] = 0;
        ?>
        <table class="tbl-cart" cellpadding="10" cellspacing="1">
            <tbody>
            <tr>
                <th style="text-align:left;">Product</th>
                <th style="text-align:left;">Artikel</th>
                <th style="text-align:right;" width="5%">Hoeveelheid</th>
                <th style="text-align:right;" width="10%">Productprijs</th>
                <th style="text-align:right;" width="10%">Prijs</th>
                <th style="text-align:center;" width="5%">Verwijderen</th>
            </tr>
            <?php
            foreach ($_SESSION["cart_item"] as $item){
                $item_price = $item["quantity"]*$item["unit_price"];
                ?>
                <tr>
                    <td><img src="<?php echo $item["img"]; ?>" class="cart-item-image" /><?php echo $item["product"]; ?></td>
                    <td><?php echo $item["code"]; ?></td>
                    <td style="text-align:right;"><?php echo $item["quantity"]; ?></td>
                    <td  style="text-align:right;"><?php echo "€ ".$item["unit_price"]; ?></td>
                    <td  style="text-align:right;"><?php echo "€ ". number_format($item_price,2); ?></td>
                    <td style="text-align:center;"><a href="/views/cart.view.php?action=remove&code=<?php echo $item["code"]; ?>" class="btnRemoveAction"><img src="../images/icon-delete.png" alt="Remove Item" /></a></td>
                </tr>



                <?php
                $total_quantity += $item["quantity"];
                $total_price += ($item["unit_price"]*$item["quantity"]);
                $_SESSION['total_quantity'] += $item["quantity"];
                $_SESSION['total_price'] += ($item["unit_price"]*$item["quantity"]);
            }
            ?>


            <tr>
                <th style="text-align:left;"></th>
                <th style="text-align:left;"></th>
                <th style="text-align:right;"><strong><?php echo $total_quantity; ?></strong></th>
                <th style="text-align:left;"></th>
                <th style="text-align:left;"></th>
                <th style="text-align:left;"></th>
            </tr>


            <?php if (@$_SESSION["User_type_id"] == 3) {
            ?>
                                    <!--   REPLACE THE 0.09 with the DATABASE TAX VALUE   -->
            <tr>
                <td align="right"></td>
                <td align="right"></td>
                <td align="right"></td>
                <td align="right">Bedrag excl. BTW:</td>
                <td align="right" colspan="1"><strong><?php echo "€ ".
                            number_format(($total_price * (1 - 0.09)), 2); ?></strong></td>
                <td align="right"></td>
            </tr>

            <!--   REPLACE THE 0.09 with the DATABASE TAX VALUE   -->
            <tr>
                    <td align="right"></td>
                    <td align="right"></td>
                    <td align="right"></td>
                <td align="right">Bedrag BTW:</td>
                <td align="right" colspan="1"><strong><?php echo "€ ".
                            number_format(($total_price * 0.09),
                                2); ?></strong></td>
                    <td align="right"></td>
            </tr>

           <?php } ?>

            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td align="right">Totaal:</td>
                <td align="right" colspan="1"><strong><?php echo "€ ".number_format($total_price, 2); ?></strong></td>

                <td></td>
            </tr>
            </tbody>
        </table>
        <?php
    } else {
        ?>
        <div class="no-records">Uw winkelwagen is leeg</div>
        <?php
    }
    ?>
</div>

<form action="verzenden" method="post">
    <button type="submit">Bestel</button>
</form>


</BODY>
</HTML>