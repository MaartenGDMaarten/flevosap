<?php

class Router {
    //    protected routes
    protected $routes = [];

    public static function load($file) {
        $router = new static; //$router = new Router;
        require $file;
        return $router;
    }

    public function define($routes) {
        $this->routes = $routes;
    }


    public function get($uri, $controller) {
        $this->routes['GET'][$uri] = $controller;
    }

    public function post($uri, $controller) {
        $this->routes['POST'][$uri] = $controller;
    }

    //  method direct
    public function direct($uri, $requestType) {
        if(array_key_exists($uri, $this->routes[$requestType])){
            return $this->callAction(...explode('@', $this->routes[$requestType][$uri]));
        }

        else {
//            die(var_dump($uri));
//            throw new Exception('Route not defined');
            header("Location: not_found");
        }
    }

    protected function callAction($controller, $action) {
        $controller = new $controller;
        if(!method_exists($controller, $action)) {
            throw new Exception('Method does not exist.');
        }
        return $controller->$action();
    }
}








